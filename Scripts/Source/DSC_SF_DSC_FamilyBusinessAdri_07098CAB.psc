;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 4
Scriptname DSC_SF_DSC_FamilyBusinessAdri_07098CAB Extends Scene Hidden

;BEGIN FRAGMENT Fragment_3
Function Fragment_3()
;BEGIN CODE
If PlayerRef.WornHasKeyword(libs.zad_DeviousHeavyBondage)
  DSC_FamilyBusinessStillTied.Start()
Else
  DSC_FamilyBusinessPlayerEscaped.Start()
EndIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN CODE
AdrianneRef.MoveTo(AdrianneMarker)
DoorRef.Lock(False)
DoorRef.SetOpen(True)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectReference Property AdrianneMarker  Auto  

Actor Property AdrianneRef  Auto  

ObjectReference Property DoorRef  Auto  

Actor Property PlayerRef  Auto  

Scene Property DSC_FamilyBusinessStillTied  Auto  

zadlibs Property libs  Auto  

Scene Property DSC_FamilyBusinessPlayerEscaped  Auto  
