;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__07082D4B Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Actor[] sexActors = new Actor[2]
sexActors[0] = PlayerRef
sexActors[1] = Alias_Lod.GetActorRef()

sslBaseAnimation[] anims = SexLab.GetAnimationsByTags(2, "Doggy,Doggystyle,Doggy Style", requireAll = False)

DSC_QuestScript.RegisterForModEvent("AnimationEnd_LodFirstSex", "LodFirstSex")

SexLab.StartSex(sexActors, anims, hook = "LodFirstSex")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ReferenceAlias Property Alias_Lod  Auto  

Actor Property PlayerRef  Auto  

DSC_DA03StartSex Property DSC_QuestScript  Auto  

SexLabFramework Property SexLab  Auto  
