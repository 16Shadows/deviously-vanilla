;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__070A1F44 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Int Count = util.PlayerRef.GetItemCount(Gold001)
If Count > 300
  Count = 300
EndIf
util.PlayerRef.RemoveItem(Gold001, Count)

util.EquipRandomGag(util.PlayerRef)

GetOwningQuest().Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

MiscObject Property Gold001  Auto  

DSC_Utility Property Util  Auto  
