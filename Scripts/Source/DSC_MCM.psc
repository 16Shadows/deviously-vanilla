Scriptname DSC_MCM extends SKI_ConfigBase

import PapyrusUtil

DSC_Utility Property util  Auto

;;;; Variable declarations
Actor Property PlayerRef                     Auto
Armor Property DSC_MG02Amulet                Auto
Armor Property MG02Amulet                    Auto
Armor Property ClavicusVileMask              Auto
Armor Property DSC_ClavicusVileMaskInventory Auto
Armor Property DSC_TrickClavicusVileMask Auto

;;; General
Bool           Property GeneralAllowRape       = FALSE Auto
GlobalVariable Property DSC_GeneralAllowRape           Auto
Bool           Property GeneralFollowersMatter       = TRUE Auto
GlobalVariable Property DSC_GeneralFollowersMatter           Auto
Bool           Property GeneralUseOrcs         = FALSE Auto
Bool           Property GeneralUseKhajiit      = FALSE Auto
Bool           Property GeneralUseArgonians    = FALSE Auto
Bool           Property GeneralUseMales        = FALSE Auto
Bool           Property GeneralUseHeavy        = FALSE Auto
Bool           Property GeneralUseChastity     = TRUE  Auto
Bool           Property GeneralUseGag          = TRUE  Auto
Bool           Property GeneralUseBlindfold    = TRUE  Auto
Bool           Property GeneralUseMittens      = TRUE  Auto
Bool           Property GeneralUseHobble       = FALSE Auto
Bool           Property GeneralUseStrictHobble = FALSE Auto

;;; Universal Equip
Bool  Property UEEnabled         = FALSE Auto
Bool  Property UENeedPlayable    = TRUE  Auto
Bool  Property UEUseTag          = FALSE Auto
Float Property UEChance          = 50.0  Auto
Bool  Property UEUseHeavy        = TRUE  Auto
Bool  Property UEUseChastity     = FALSE Auto
Bool  Property UEUseGag          = TRUE  Auto
Bool  Property UEUseBlindfold    = TRUE  Auto
Bool  Property UEUseMittens      = TRUE  Auto
Bool  Property UEUseHobble       = FALSE Auto
Bool  Property UEUseStrictHobble = FALSE Auto

;;; Fit For a Jarl
String[]       FitForAJarlDressStrings
String[]       FitForAJarlDressShortStrings
Armor[]        FitForAJarlDressDevices
Armor Property FitForAJarlDevice             Auto
Int            _FitForAJarlDeviceIdx

;;; Wabbajack
Bool  Property WJUseHeavy        = FALSE Auto
Bool  Property WJUseChastity     = TRUE  Auto
Bool  Property WJUseGag          = TRUE  Auto
Bool  Property WJUseBlindFold    = TRUE  Auto
Bool  Property WJUseMittens      = FALSE Auto
Bool  Property WJUseHobble       = FALSE Auto
Bool  Property WJUseStrictHobble = FALSE Auto
Float Property WJChanceSelf      = 4.0   Auto
Float Property WJChanceOthers    = 4.0   Auto

;;; Saarthal Collar
Bool           Property SCAllowAtronachs                 = TRUE Auto
GlobalVariable Property DSC_SaarthalCollarAllowAtronachs        Auto
Bool           Property SCAllowRepeated                  = TRUE Auto

;;; Clavicus Vile Mask
Bool           Property CVMaskEnabled        = TRUE Auto
Int            Property CVGagWeight          = 50   Auto
Int            Property CVBlindWeight        = 50   Auto
Int            Property CVCollarWeight       = 50   Auto
Float          Property CVMinTimeUntilToggle = 8.0  Auto
Float          Property CVMaxTimeUntilToggle = 16.0 Auto
Bool           Property CVIModEnabled        = TRUE Auto
Int            Property CVEventsNeeded       = 20   Auto
GlobalVariable Property DSC_ClavicusVileEventsDone  Auto
GlobalVariable Property DSC_ClavicusVileAllowEscape Auto

;;; Climb the steps
Bool 		   Property ClimbTheStepsTease = False Auto
Bool		   Property ClimbTheStepsConstant = False Auto
Int		   	   Property ClimbTheStepsFrequency = 30 Auto
Int			   Property ClimbTheStepsDuration = 5	 Auto

;;; SS Drevis Neloren Introduction
String[]          DrevisGagStrings
String[]          DrevisGagDevices
String   Property DrevisGagDevice    = "gag,ring"  Auto
Bool     Property DrevisHeavyBondage = FALSE       Auto
Int               _DrevisGagIdx

;;; SCON Vasha Release
Bool Property VashaRopeBinder = TRUE  Auto
Bool Property VashaHood       = TRUE  Auto

Int Function GetVersion()
  ;;; Script Version 2
  ; - Added restart quests page
  Return 2
EndFunction

Event OnVersionUpdate(Int Version)
  If Version >= 2 && CurrentVersion < 2
    Pages = PushString(Pages, "Restart quests")
  EndIf
EndEvent

Event OnConfigInit()
  Pages = new string[2]
  Pages[0] = "General"
  Pages[1] = "Vanilla Quests"
  If Game.GetModByName("Deviously Vanilla - SexLab Solutions.esp") != 255 || Game.GetModByName("Deviously Vanilla - AIO.esp") != 255
    Pages = PushString(Pages, "SexLab Solutions")
  EndIf
  If Game.GetModByName("Deviously Vanilla - SexLab Confabulation.esp") != 255 || Game.GetModByName("Deviously Vanilla - AIO.esp") != 255
    Pages = PushString(Pages, "SexLab Confabulation")
  EndIf
  ; Pages = PushString(Pages, "Restart quests")

  FitForAJarlDressStrings = new String[4]
  FitForAJarlDressStrings[0] = "Elegant Hobble Dress (Relaxed)"
  FitForAJarlDressStrings[1] = "Relaxed Hobble Dress"
  FitForAJarlDressStrings[2] = "Strict Hobble Dress"
  FitForAJarlDressStrings[3] = "Strict Hobble Dress with Straitjacket"

  FitForAJarlDressShortStrings = new String[4]
  FitForAJarlDressShortStrings[0] = "Elegant"
  FitForAJarlDressShortStrings[1] = "Relaxed"
  FitForAJarlDressShortStrings[2] = "Strict"
  FitForAJarlDressShortStrings[3] = "Straitjacket"

  FitForAJarlDressDevices = new Armor[4]
  FitForAJarlDressDevices[0] = util.FitForAJarlElegant
  FitForAJarlDressDevices[1] = util.FitForAJarlRelaxed
  FitForAJarlDressDevices[2] = util.FitForAJarlStrict
  FitForAJarlDressDevices[3] = util.FitForAJarlStraitjacket

  DrevisGagStrings = new String[3]
  DrevisGagStrings[0] = "Ring gag"
  DrevisGagStrings[1] = "Ball gag"
  DrevisGagStrings[2] = "Panel gag"

  DrevisGagDevices = new String[3]
  DrevisGagDevices[0] = "gag,ring"
  DrevisGagDevices[1] = "gag,ball"
  DrevisGagDevices[2] = "gag,panel"
EndEvent

; State DumpListsST
;   Event OnSelectST()
;     util.DumpLists()
;   EndEvent
; EndState

Event OnPageReset(string page)
  If page == "General" || page == ""
    SetCursorFillMode(TOP_TO_BOTTOM)
    SetCursorPosition(0)

    AddTextOptionST("StopQuestsST", "Safeword", "Release me!")
    ; AddTextOptionST("DumpListsST", "test dont use", "")

    AddHeaderOption("General options")
    AddToggleOptionST("GeneralAllowRapeST"       , "Allow rape scenes"           , GeneralAllowRape)
	AddToggleOptionST("GeneralFollowersMatterST",  "Followers matter"			 , GeneralFollowersMatter)
    AddToggleOptionST("GeneralUseOrcsST"         , "Allow orcs"                  , GeneralUseOrcs)
    AddToggleOptionST("GeneralUseKhajiitST"      , "Allow Khajiit"               , GeneralUseKhajiit)
    AddToggleOptionST("GeneralUseArgoniansST"    , "Allow Argonians"             , GeneralUseArgonians)
    AddToggleOptionST("GeneralUseMalesST"        , "Allow men"                   , GeneralUseMales)
    ;AddHeaderOption("General device options")
    AddEmptyOption()
    AddToggleOptionST("GeneralUseHeavyST"        , "Allow heavy bondage"         , GeneralUseHeavy)
    AddToggleOptionST("GeneralUseChastityST"     , "Allow chastity"              , GeneralUseChastity)
    AddToggleOptionST("GeneralUseGagST"          , "Allow gags"                  , GeneralUseGag)
    AddToggleOptionST("GeneralUseBlindfoldST"    , "Allow blindfolds"            , GeneralUseBlindfold)
    AddToggleOptionST("GeneralUseMittensST"      , "Allow bondage mittens"       , GeneralUseMittens)
    AddToggleOptionST("GeneralUseHobbleST"       , "Allow hobble skirts"         , GeneralUseHobble)
	If GeneralUseHobble
		AddToggleOptionST("GeneralUseStrictHobbleST" , "Allow strict hobble skirts"  , GeneralUseStrictHobble)
	Else
		AddToggleOptionST("GeneralUseStrictHobbleST" , "Allow strict hobble skirts"  , GeneralUseStrictHobble, a_flags = OPTION_FLAG_DISABLED)
	Endif
	
    SetCursorPosition(1)

    AddHeaderOption  ("Equip on aggressive sex")
    AddToggleOptionST("UEEnabledST"         , "Enable this feature"         , UEEnabled)
    AddToggleOptionST("UENeedPlayableST"    , "Need playable race"          , UENeedPlayable)
    AddToggleOptionST("UEUseTagST"          , "Use Aggressive tag"          , UEUseTag)
    AddSliderOptionST("UEChanceST"          , "Chance a device is equipped" , UEChance, "{1}%")
    AddEmptyOption()
    AddToggleOptionST("UEUseHeavyST"        , "Allow heavy bondage"         , UEUseHeavy)
    AddToggleOptionST("UEUseChastityST"     , "Allow chastity"              , UEUseChastity)
    AddToggleOptionST("UEUseGagST"          , "Allow gags"                  , UEUseGag)
    AddToggleOptionST("UEUseBlindfoldST"    , "Allow blindfolds"            , UEUseBlindfold)
    AddToggleOptionST("UEUseMittensST"      , "Allow bondage mittens"       , UEUseMittens)
    AddToggleOptionST("UEUseHobbleST"       , "Allow hobble skirts"         , UEUseHobble)
	If UEUseHobble
		AddToggleOptionST("UEUseStrictHobbleST" , "Allow strict hobble skirts"  , UEUseStrictHobble)
	Else
		AddToggleOptionST("UEUseStrictHobbleST" , "Allow strict hobble skirts"  , UEUseStrictHobble, a_flags = OPTION_FLAG_DISABLED)
	Endif
  ElseIf page == "Vanilla Quests"
    SetCursorFillMode(TOP_TO_BOTTOM)
    AddHeaderOption  ("Wabbajack")
    AddSliderOptionST("WJChanceSelfST"      , "Chance on self"              , WJChanceSelf   , "{1}%")
    AddSliderOptionST("WJChanceOthersST"    , "Chance on others"            , WJChanceOthers , "{1}%")
    AddToggleOptionST("WJUseHeavyST"        , "Allow heavy bondage"         , WJUseHeavy)
    AddToggleOptionST("WJUseChastityST"     , "Allow chastity"              , WJUseChastity)
    AddToggleOptionST("WJUseGagST"          , "Allow gags"                  , WJUseGag)
    AddToggleOptionST("WJUseBlindfoldST"    , "Allow blindfolds"            , WJUseBlindfold)
    AddToggleOptionST("WJUseMittensST"      , "Allow bondage mittens"       , WJUseMittens)
    AddToggleOptionST("WJUseHobbleST"       , "Allow hobble skirts"         , WJUseHobble)
    If WJUseHobble
		AddToggleOptionST("WJUseStrictHobbleST" , "Allow strict hobble skirts"  , WJUseStrictHobble)
	Else
		AddToggleOptionST("WJUseStrictHobbleST" , "Allow strict hobble skirts"  , WJUseStrictHobble, a_flags = OPTION_FLAG_DISABLED)
	Endif

	AddHeaderOption	 ("Climb The Steps")
	AddToggleOptionST("ClimbTheStepsTeaseST", 	 "Tease only"				, ClimbTheStepsTease)
	AddToggleOptionST("ClimbTheStepsConstantST", "Constant vibration"		, ClimbTheStepsConstant)
	If ClimbTheStepsConstant
		AddSliderOptionST("ClimbTheStepsFrequencyST", "Vibration frequency" 	, ClimbTheStepsFrequency, "{1} seconds", a_flags = OPTION_FLAG_DISABLED)
		AddSliderOptionST("ClimbTheStepsDurationST", "Vibration duration" 		, ClimbTheStepsDuration, "{1} seconds", a_flags = OPTION_FLAG_DISABLED)
	Else
		AddSliderOptionST("ClimbTheStepsFrequencyST", "Vibration frequency" 	, ClimbTheStepsFrequency, "{1} seconds")
		AddSliderOptionST("ClimbTheStepsDurationST", "Vibration duration" 		, ClimbTheStepsDuration, "{1} seconds")
	Endif
	
    SetCursorPosition(1)
    AddHeaderOption  ("Saarthal Collar")
    AddToggleOptionST("SCAllowAtronachsST"  , "Allow atronachs"             , SCAllowAtronachs)
    AddToggleOptionST("SCAllowRepeatedST"   , "Repeatable quest"            , SCAllowRepeated)

    AddHeaderOption  ("Fit For a Jarl")
    AddMenuOptionST  ("FitForAJarlDeviceST" , "Dress type", FitForAJarlDressShortStrings[_FitForAJarlDeviceIdx])

    AddHeaderOption  ("Clavicus Vile Mask")
    AddToggleOptionST("CVMaskEnabledST"        , "Enable the devious mask"   , CVMaskEnabled)
	AddSliderOptionST("CVEventsNeededST"	   , "Events to unlock"			 , CVEventsNeeded)
    AddToggleOptionST("CVIModEnabledST"        , "Use hypno effect"          , CVIModEnabled)
    AddSliderOptionST("CVGagWeightST"          , "Gag weight"                , CVGagWeight)
    AddSliderOptionST("CVBlindWeightST"        , "Blindfold weight"          , CVBlindWeight)
    AddSliderOptionST("CVCollarWeightST"       , "Collar weight"             , CVCollarWeight)
    AddSliderOptionST("CVMinTimeUntilToggleST" , "Minimum time until toggle" , CVMinTimeUntilToggle, "{1} hours")
    AddSliderOptionST("CVMaxTimeUntilToggleST" , "Maximum time until toggle" , CVMaxTimeUntilToggle, "{1} hours")
	
  ElseIf (page == "SexLab Solutions")
    SetCursorFillMode(TOP_TO_BOTTOM)
    SetCursorPosition(0)

    AddHeaderOption  ("Drevis Neloren Introduction")
    AddMenuOptionST  ("DrevisGagST", "Gag type", DrevisGagStrings[_DrevisGagIdx])
    AddToggleOptionST("DrevisHeavyBondageST", "Use heavy bondage", DrevisHeavyBondage)
  ElseIf page == "SexLab Confabulation"
    SetCursorFillMode(TOP_TO_BOTTOM)
    SetCursorPosition(0)

    AddHeaderOption  ("Vasha Release (Dark Brotherhood)")
    AddToggleOptionST("VashaRopeBinderST", "Use rope armbinder", VashaRopeBinder)
    AddToggleOptionST("VashaHoodST", "Use hood", VashaHood)
  ElseIf page == "Restart quests"
    SetCursorFillMode(TOP_TO_BOTTOM)
    SetCursorPosition(0)

    AddTextOptionST("FitForAJarlResetST", "Fit For a Jarl", "Restart")
    AddTextOptionST("AdrianneSwordResetST", "Adrianne greatsword delivery", "Restart")
  EndIf
EndEvent

;;;; General
State StopQuestsST
  Event OnSelectST()
    Int handle = ModEvent.Create("DSC_Quest_SIGTERM")
    If handle
      ModEvent.Send(handle)
      Debug.Trace("DSC: Sending SIGTERM event to Deviously Vanilla quests")
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Shuts down Deviously Vanilla quests and removes quest items. Doesn't remove other devices.")
  EndEvent
EndState

State GeneralAllowRapeST
  Event OnSelectST()
    GeneralAllowRape = !GeneralAllowRape
    SetToggleOptionValueST(GeneralAllowRape)
    DSC_GeneralAllowRape.SetValue(GeneralAllowRape as Float)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow Deviously Vanilla to play rape scenes. Doesn't stop other mods like Solutions or Confabulation from doing this.\n\nRecommended to enable, but turn off if you don't want to see them.")
  EndEvent
EndState

State GeneralFollowersMatterST
  Event OnSelectST()
    GeneralFollowersMatter = !GeneralFollowersMatter
    SetToggleOptionValueST(GeneralFollowersMatter)
    DSC_GeneralFollowersMatter.SetValue(GeneralFollowersMatter as Float)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Modifies some scenarios depending on presence/absence of followers.\n\nIf disabled, some scenes may not make sense with follower(s) around")
  EndEvent
EndState

State GeneralUseOrcsST
  Event OnSelectST()
    GeneralUseOrcs = !GeneralUseOrcs
    SetToggleOptionValueST(GeneralUseOrcs)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow devices on Orc NPCs.")
  EndEvent
EndState

State GeneralUseKhajiitST
  Event OnSelectST()
    GeneralUseKhajiit = !GeneralUseKhajiit
    SetToggleOptionValueST(GeneralUseKhajiit)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow devices on Khajiit NPCs.")
  EndEvent
EndState

State GeneralUseArgoniansST
  Event OnSelectST()
    GeneralUseArgonians = !GeneralUseArgonians
    SetToggleOptionValueST(GeneralUseArgonians)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow devices on Argonian NPCs.")
  EndEvent
EndState

State GeneralUseMalesST
  Event OnSelectST()
    GeneralUseMales = !GeneralUseMales
    SetToggleOptionValueST(GeneralUseMales)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow devices on male NPCs")
  EndEvent
EndState

State GeneralUseHeavyST
  Event OnSelectST()
    GeneralUseHeavy = !GeneralUseHeavy
    SetToggleOptionValueST(GeneralUseHeavy)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of heavy bondage (Armbinders, yokes etc).\nThese settings are overwritten by other selections for specific scenarios (such as the Wabbajack)")
  EndEvent
EndState

State GeneralUseChastityST
  Event OnSelectST()
    GeneralUseChastity = !GeneralUseChastity
    SetToggleOptionValueST(GeneralUseChastity)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of chastity belts, bras and harnesses.\nThese settings are overwritten by other selections for specific scenarios (such as the Wabbajack)")
  EndEvent
EndState

State GeneralUseGagST
  Event OnSelectST()
    GeneralUseGag = !GeneralUseGag
    SetToggleOptionValueST(GeneralUseGag)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of gags.\nThese settings are overwritten by other selections for specific scenarios (such as the Wabbajack)")
  EndEvent
EndState

State GeneralUseBlindfoldST
  Event OnSelectST()
    GeneralUseBlindfold = !GeneralUseBlindfold
    SetToggleOptionValueST(GeneralUseBlindfold)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of blindfolds.\nThese settings are overwritten by other selections for specific scenarios (such as the Wabbajack)")
  EndEvent
EndState

State GeneralUseMittensST
  Event OnSelectST()
    GeneralUseMittens = !GeneralUseMittens
    SetToggleOptionValueST(GeneralUseMittens)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of bondage mittens.\nThese settings are overwritten by other selections for specific scenarios (such as the Wabbajack)")
  EndEvent
EndState

State GeneralUseHobbleST
  Event OnSelectST()
    GeneralUseHobble = !GeneralUseHobble
    SetToggleOptionValueST(GeneralUseHobble)

    If GeneralUseHobble
      SetOptionFlagsST(OPTION_FLAG_NONE, a_StateName = "GeneralUseStrictHobbleST")
    Else
      SetOptionFlagsST(OPTION_FLAG_DISABLED, a_StateName = "GeneralUseStrictHobbleST")
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of hobble skirts.\nThese settings are overwritten by other selections for specific scenarios (such as the Wabbajack)")
  EndEvent
EndState

State GeneralUseStrictHobbleST
  Event OnSelectST()
    GeneralUseStrictHobble = !GeneralUseStrictHobble
    SetToggleOptionValueST(GeneralUseStrictHobble)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of strict hobble skirts.\nThese settings are overwritten by other selections for specific scenarios (such as the Wabbajack)")
  EndEvent
EndState

;;; Universal Equip
State UEEnabledST
  Event OnSelectST()
    UEEnabled = !UEEnabled
    SetToggleOptionValueST(UEEnabled)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Enable this feature. Might break other mods if a device is equipped when it is not expected (So probably disable this during POP arrest and such), be careful using it.")
  EndEvent
EndState

State UENeedPlayableST
  Event OnSelectST()
    UENeedPlayable = !UENeedPlayable
    SetToggleOptionValueST(UENeedPlayable)
  EndEvent

  Event OnHighlightST()
    SetInfoText("If enabled, sex scene needs to have an actor in it that is a playable race. This way animals don't equip devices, but it might not work with followers and such that use custom races.")
  EndEvent
EndState

State UEUseTagST
  Event OnSelectST()
    UEUseTag = !UEUseTag
    SetToggleOptionValueST(UEUseTag)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Also use this feature on consensual animations using aggressive of rough tags, while in the receiving position.")
  EndEvent
EndState

State UEChanceST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(UEChance)
    SetSliderDialogDefaultValue(50.0)
    SetSliderDialogRange(0, 100)
    SetSliderDialogInterval(0.5)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    UEChance = a_value
    SetSliderOptionValueST(UEChance, "{1}%")
  EndEvent

  Event OnHighlightST()
    SetInfoText("Chance for a device to be equipped after aggressive sex")
  EndEvent
EndState

State UEUseHeavyST
  Event OnSelectST()
    UEUseHeavy = !UEUseHeavy
    SetToggleOptionValueST(UEUseHeavy)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of heavy bondage (Armbinders, yokes etc).")
  EndEvent
EndState

State UEUseChastityST
  Event OnSelectST()
    UEUseChastity = !UEUseChastity
    SetToggleOptionValueST(UEUseChastity)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of chastity belts, bras and harnesses.")
  EndEvent
EndState

State UEUseGagST
  Event OnSelectST()
    UEUseGag = !UEUseGag
    SetToggleOptionValueST(UEUseGag)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of gags")
  EndEvent
EndState

State UEUseBlindfoldST
  Event OnSelectST()
    UEUseBlindfold = !UEUseBlindfold
    SetToggleOptionValueST(UEUseBlindfold)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of blindfolds")
  EndEvent
EndState

State UEUseMittensST
  Event OnSelectST()
    UEUseMittens = !UEUseMittens
    SetToggleOptionValueST(UEUseMittens)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of bondage mittens")
  EndEvent
EndState

State UEUseHobbleST
  Event OnSelectST()
    UEUseHobble = !UEUseHobble
    SetToggleOptionValueST(UEUseHobble)

    If UEUseHobble
      SetOptionFlagsST(OPTION_FLAG_NONE, a_StateName = "UEUseStrictHobbleST")
    Else
      SetOptionFlagsST(OPTION_FLAG_DISABLED, a_StateName = "UEUseStrictHobbleST")
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of hobble skirts")
  EndEvent
EndState

State UEUseStrictHobbleST
  Event OnSelectST()
    UEUseStrictHobble = !UEUseStrictHobble
    SetToggleOptionValueST(UEUseStrictHobble)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of strict hobble skirts")
  EndEvent
EndState


;;;; Categories
;;; Fit For a Jarl
State FitForAJarlDeviceST
  Event OnMenuOpenST()
    SetMenuDialogStartIndex(_FitForAJarlDeviceIdx)
    SetMenuDialogDefaultIndex(0)
    SetMenuDialogOptions(FitForAJarlDressStrings)
  EndEvent

  Event OnMenuAcceptST(int a_index)
    _FitForAJarlDeviceIdx = a_index
    FitForAJarlDevice = FitForAJarlDressDevices[a_index]
    SetMenuOptionValueST(FitForAJarlDressShortStrings[a_index])
  EndEvent

  Event OnDefaultST()
    _FitForAJarlDeviceIdx = 0
    FitForAJarlDevice = FitForAJarlDressDevices[0]
    SetMenuOptionValueST(FitForAJarlDressShortStrings[0])
  EndEvent
EndState

;;; Wabbajack
State WJUseHeavyST
  Event OnSelectST()
    WJUseHeavy = !WJUseHeavy
    SetToggleOptionValueST(WJUseHeavy)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of heavy bondage (Armbinders, yokes etc).")
  EndEvent
EndState

State WJUseChastityST
  Event OnSelectST()
    WJUseChastity = !WJUseChastity
    SetToggleOptionValueST(WJUseChastity)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of chastity belts and bras.")
  EndEvent
EndState

State WJUseGagST
  Event OnSelectST()
    WJUseGag = !WJUseGag
    SetToggleOptionValueST(WJUseGag)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of gags")
  EndEvent
EndState

State WJUseBlindfoldST
  Event OnSelectST()
    WJUseBlindfold = !WJUseBlindfold
    SetToggleOptionValueST(WJUseBlindfold)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of blindfolds")
  EndEvent
EndState

State WJUseMittensST
  Event OnSelectST()
    WJUseMittens = !WJUseMittens
    SetToggleOptionValueST(WJUseMittens)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of bondage mittens")
  EndEvent
EndState

State WJUseHobbleST
  Event OnSelectST()
    WJUseHobble = !WJUseHobble
    SetToggleOptionValueST(WJUseHobble)

    If WJUseHobble
      SetOptionFlagsST(OPTION_FLAG_NONE, a_StateName = "WJUseStrictHobbleST")
    Else
      SetOptionFlagsST(OPTION_FLAG_DISABLED, a_StateName = "WJUseStrictHobbleST")
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of hobble skirts")
  EndEvent
EndState

State WJUseStrictHobbleST
  Event OnSelectST()
    WJUseStrictHobble = !WJUseStrictHobble
    SetToggleOptionValueST(WJUseStrictHobble)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the selection of strict hobble skirts")
  EndEvent
EndState

State WJChanceSelfST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(WJChanceSelf)
    SetSliderDialogDefaultValue(4.0)
    SetSliderDialogRange(0, 100)
    SetSliderDialogInterval(0.5)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    WJChanceSelf = a_value
    SetSliderOptionValueST(WJChanceSelf, "{1}%")
  EndEvent

  Event OnHighlightST()
    SetInfoText("Chance the Wabbajack equips a device on the player. This will be calculated independently, so it can still trigger effects on the target. (excluding devices, because of mutex in DDi)")
  EndEvent
EndState

State WJChanceOthersST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(WJChanceOthers)
    SetSliderDialogDefaultValue(4.0)
    SetSliderDialogRange(0, 100)
    SetSliderDialogInterval(0.5)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    WJChanceOthers = a_value
    SetSliderOptionValueST(WJChanceOthers, "{1}%")
  EndEvent

  Event OnHighlightST()
    SetInfoText("Chance the Wabbajack equips a device on the target. This is calculated before normal effects, so a 100% chance will never apply normal effects.")
  EndEvent
EndState

;;; Climb The Steps

State ClimbTheStepsTeaseST
	Event OnSelectST()
		ClimbTheStepsTease = !ClimbTheStepsTease
		SetToggleOptionValueST(ClimbTheStepsTease)
	EndEvent
	
	Event OnHighlightST()
		SetInfoText("If on, the plugs won't allow PC to orgasm")
	EndEvent
EndState

State ClimbTheStepsConstantST
	Event OnSelectST()
		ClimbTheStepsConstant = !ClimbTheStepsConstant
		SetToggleOptionValueST(ClimbTheStepsConstant)
		If ClimbTheStepsConstant
			SetOptionFlagsST(OPTION_FLAG_DISABLED, true, "ClimbTheStepsFrequencyST")
			SetOptionFlagsST(OPTION_FLAG_DISABLED, false, "ClimbTheStepsDurationST")
		Else
			SetOptionFlagsST(OPTION_FLAG_NONE, true, "ClimbTheStepsFrequencyST")
			SetOptionFlagsST(OPTION_FLAG_NONE, false, "ClimbTheStepsDurationST")
		Endif
	EndEvent
	
	Event OnHighlightST()
		SetInfoText("If on, the plugs will vibrate constantly")
	EndEvent
EndState

State ClimbTheStepsFrequencyST
	Event OnSliderOpenST()
		SetSliderDialogStartValue(ClimbTheStepsFrequency)
		SetSliderDialogDefaultValue(30)
		SetSliderDialogRange(1, 600)
		SetSliderDialogInterval(1)
	EndEvent
	
	Event OnSliderAcceptST(Float a_value)
		ClimbTheStepsFrequency = a_value as Int
		SetSliderOptionValueST(ClimbTheStepsFrequency, "{1} seconds")
	EndEvent
	
	Event OnHighlightST()
		SetInfoText("How often the plugs will vibrate\n\nPlugs won't trigger in combat")
	EndEvent
EndState

State ClimbTheStepsDurationST
	Event OnSliderOpenST()
		SetSliderDialogStartValue(ClimbTheStepsDuration)
		SetSliderDialogDefaultValue(5)
		SetSliderDialogRange(1, 60)
		SetSliderDialogInterval(1)
	EndEvent
	
	Event OnSliderAcceptST(Float a_value)
		ClimbTheStepsDuration = a_value as Int
		SetSliderOptionValueST(ClimbTheStepsDuration, "{1} seconds")
	EndEvent
	
	Event OnHighlightST()
		SetInfoText("How long the plugs will vibrate\n\nCombat won't stop the effect")
	EndEvent
EndState

;;; Saarthal Collar
State SCAllowAtronachsST
  Event OnSelectST()
    SCAllowAtronachs = !SCAllowAtronachs
    SetToggleOptionValueST(SCAllowAtronachs)
    DSC_SaarthalCollarAllowAtronachs.SetValue(SCAllowAtronachs as Float)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Disable this if you don't have any atronach SexLab animations installed, or don't want to see them.")
  EndEvent
EndState

State SCAllowRepeatedST
  Event OnSelectST()
    SCAllowRepeated = !SCAllowRepeated
    SetToggleOptionValueST(SCAllowRepeated)

    Int Count1 = PlayerRef.GetItemCount(DSC_MG02Amulet)
    Int Count2 = PlayerRef.GetItemCount(MG02Amulet)
    If !SCAllowRepeated && Count1 > 0
      PlayerRef.RemoveItem(DSC_MG02Amulet, aiCount = Count1, abSilent = True)
      PlayerRef.AddItem(MG02Amulet, aiCount = Count1, abSilent = True)
    ElseIf SCAllowRepeated && Count2 > 0
      PlayerRef.RemoveItem(MG02Amulet, aiCount = Count2, abSilent = True)
      PlayerRef.AddItem(DSC_MG02Amulet, aiCount = Count2, abSilent = True)
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Allow the collar to appear again if you reequip the amulet.\n\nNote: If you already did the quest, this will only work if you have the amulet or collar in your inventory.")
  EndEvent
EndState

;;; Clavicus Vile Mask
State CVMaskEnabledST
  Event OnSelectST()
    ;; guard clause to prevent breaking quest if it's running.
    If Quest.GetQuest("DSC_ClavicusVileMaskQuest").IsRunning()
      Return
    EndIf
    CVMaskEnabled = !CVMaskEnabled
    SetToggleOptionValueST(CVMaskEnabled)

    Int Count0 = PlayerRef.GetItemCount(DSC_TrickClavicusVileMask)
    Int Count1 = PlayerRef.GetItemCount(DSC_ClavicusVileMaskInventory)
    Int Count2 = PlayerRef.GetItemCount(ClavicusVileMask)
    If !CVMaskEnabled && Count1 + Count0 > 0
      PlayerRef.RemoveItem(DSC_TrickClavicusVileMask, aiCount = Count0, abSilent = True)
      PlayerRef.RemoveItem(DSC_ClavicusVileMaskInventory, aiCount = Count1, abSilent = True)
      PlayerRef.AddItem(ClavicusVileMask, aiCount = Count1 + Count0, abSilent = True)
    ElseIf CVMaskEnabled && Count2 > 0
      PlayerRef.RemoveItem(ClavicusVileMask, aiCount = Count2, abSilent = True)
      PlayerRef.AddItem(DSC_TrickClavicusVileMask, aiCount = Count2, abSilent = True)
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Toggle this quest. Won't work if mask is equipped, use safeword if needed.\nNote: Only toggle this if the mask is in your inventory, or if you haven't gotten the mask yet\nCW: Has content that could be considered rape which is not disabled by the option under General.")
  EndEvent
EndState

State CVIModEnabledST
  Event OnSelectST()
    CVIModEnabled = !CVIModEnabled
    SetToggleOptionValueST(CVIModEnabled)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Toggle visual effect on hypno events. Recommended to use, but it is a white flash, so disable if you are sensitive to that kind of thing.")
  EndEvent
EndState

State CVEventsNeededST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(CVEventsNeeded)
    SetSliderDialogDefaultValue(20)
    SetSliderDialogRange(10, 100)
    SetSliderDialogInterval(1)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    CVEventsNeeded = a_value as Int
    SetSliderOptionValueST(CVEventsNeeded)
    If DSC_ClavicusVileEventsDone.GetValueInt() > CVEventsNeeded
      DSC_ClavicusVileAllowEscape.SetValueInt(True as Int)
    Else
      DSC_ClavicusVileAllowEscape.SetValueInt(False as Int)
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("The amount of hypno events are needed for them to wear off and allow you to escape the mask. Both the periodic blindfold events and dialogue events are included.")
  EndEvent
EndState

State CVGagWeightST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(CVGagWeight)
    SetSliderDialogDefaultValue(50)
    SetSliderDialogRange(0, 100)
    SetSliderDialogInterval(1)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    CVGagWeight = a_value as Int
    SetSliderOptionValueST(CVGagWeight)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Weight for the gag to be toggled (activated if it's inactive and vice versa), when the state is changed.")
  EndEvent
EndState

State CVBlindWeightST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(CVBlindWeight)
    SetSliderDialogDefaultValue(50)
    SetSliderDialogRange(0, 100)
    SetSliderDialogInterval(1)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    CVBlindWeight = a_value as Int
    SetSliderOptionValueST(CVBlindWeight)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Weight for the blindfold to be toggled (activated if it's inactive and vice versa), when the state is changed.")
  EndEvent
EndState

State CVCollarWeightST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(CVCollarWeight)
    SetSliderDialogDefaultValue(50)
    SetSliderDialogRange(0, 100)
    SetSliderDialogInterval(1)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    CVCollarWeight = a_value as Int
    SetSliderOptionValueST(CVCollarWeight)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Weight for the collar to be toggled (activated if it's inactive and vice versa), when the state is changed.")
  EndEvent
EndState

State CVMinTimeUntilToggleST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(CVMinTimeUntilToggle)
    SetSliderDialogDefaultValue(8.0)
    SetSliderDialogRange(1.0, 336.0)
    SetSliderDialogInterval(0.1)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    If a_value <= CVMaxTimeUntilToggle
      CVMinTimeUntilToggle = a_value
      SetSliderOptionValueST(CVMinTimeUntilToggle, "{1} hours")
    Else
      Debug.Messagebox("Minimum time can't be higher than maximum time.")
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Minimum time until the state of the mask is changed. Time is in game time.\nOnly takes effect after next state change.")
  EndEvent
EndState

State CVMaxTimeUntilToggleST
  Event OnSliderOpenST()
    SetSliderDialogStartValue(CVMaxTimeUntilToggle)
    SetSliderDialogDefaultValue(8.0)
    SetSliderDialogRange(1.0, 336.0)
    SetSliderDialogInterval(0.1)
  EndEvent

  Event OnSliderAcceptST(Float a_value)
    If a_value >= CVMinTimeUntilToggle
      CVMaxTimeUntilToggle = a_value
      SetSliderOptionValueST(CVMaxTimeUntilToggle, "{1} hours")
    Else
      Debug.Messagebox("Maximum time can't be lower than minimum time.")
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Maximum time until the state of the mask is changed. Time is in game time.\nOnly takes effect after next state change.")
  EndEvent
EndState


;;; Drevis Neloren Introduction
State DrevisGagST
  Event OnMenuOpenST()
    SetMenuDialogStartIndex(_DrevisGagIdx)
    SetMenuDialogDefaultIndex(0)
    SetMenuDialogOptions(DrevisGagStrings)
  EndEvent

  Event OnMenuAcceptST(int a_index)
    _DrevisGagIdx = a_index
    DrevisGagDevice = DrevisGagDevices[a_index]
    SetMenuOptionValueST(DrevisGagStrings[a_index])
  EndEvent

  Event OnDefaultST()
    _DrevisGagIdx = 0
    DrevisGagDevice = DrevisGagDevices[0]
    SetMenuOptionValueST(DrevisGagStrings[0])
  EndEvent
EndState

State DrevisHeavyBondageST
  Event OnSelectST()
    DrevisHeavyBondage = !DrevisHeavyBondage
    SetToggleOptionValueST(DrevisHeavyBondage)
  EndEvent

  Event OnHighlightST()
    SetInfoText("Drevis will also equip heavy bondage (armbinders, yokes etc)")
  EndEvent
EndState

;;; Vasha release
State VashaRopeBinderST
  Event OnSelectST()
    VashaRopeBinder= !VashaRopeBinder
    SetToggleOptionValueST(VashaRopeBinder)
  EndEvent
EndState

State VashaHoodST
  Event OnSelectST()
    VashaHood= !VashaHood
    SetToggleOptionValueST(VashaHood)
  EndEvent
EndState


;;;; Restart quests
State FitForAJarlResetST
  Event OnSelectST()
    Quest ThisQuest = Quest.GetQuest("SolitudeFreeform02")
    If !ThisQuest.IsRunning()
      ThisQuest.SetStage(0)
      ThisQuest.Start()
      SetTextOptionValueST("Done")
    Else
      SetTextOptionValueST("Already running")
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Use this to restart the Fit For a Jarl quest, so you can do it again.")
  EndEvent
EndState

State AdrianneSwordResetST
  Event OnSelectST()
    Quest Quest1 = Quest.GetQuest("FreeformWhiterunQuest03")
    Quest Quest2 = Quest.GetQuest("DSC_FamilyBusiness")
    If !Quest1.IsRunning() && !Quest2.IsRunning()
      DialogueWhiterunScript QuestScript = Quest.GetQuest("DialogueWhiterun") as DialogueWhiterunScript
      QuestScript.pSword = 0
      SetTextOptionValueST("Done")
    Else
      SetTextOptionValueST("Already running")
    EndIf
  EndEvent

  Event OnHighlightST()
    SetInfoText("Use this to restart the greatsword delivery quest of Adrianne, so you can do it again.")
  EndEvent
EndState

Bool Property GeneralUseFollower  Auto  

; For compability with previous versions
Function OnGameReload()
	Parent.OnGameReload()
	util.OnGameReload()
EndFunction