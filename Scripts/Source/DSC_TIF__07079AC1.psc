;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__07079AC1 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
questScript.RegisterForModEvent("AnimationEnd_NerienCollege", "NerienCollegeDisappear")

Actor[] actors = new Actor[2]
actors[0] = PlayerRef
actors[1] = Alias_NerienAlias.GetActorRef()

sslBaseAnimation[] anims
anims = SexLab.GetAnimationsByTags(actors.length, "Magick, Magic", RequireAll = False)

If anims == None
  anims == SexLab.GetAnimationsByTags(actors.length, "Aggressive")
EndIf

SexLab.StartSex(actors, anims, victim = PlayerRef, hook = "NerienCollege")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

SexLabFramework Property SexLab  Auto  

ReferenceAlias Property Alias_NerienAlias  Auto  

DSC_SaarthalCollarQuest Property QuestScript  Auto  
