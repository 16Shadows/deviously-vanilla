;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__07081231 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
util.EquipRandomBlindfold(PlayerRef)
DSC_MQ106DelphineEquippedBlindfold.SetValueInt(1)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property util  Auto  

Actor Property PlayerRef  Auto  

GlobalVariable Property DSC_MQ106DelphineEquippedBlindfold  Auto  
