;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0707A03E Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Actor[] sexActors = new Actor[2]
sexActors[0] = PlayerRef
sexActors[1] = SavosRef

sslBaseAnimation[] anims
anims = SexLab.GetAnimationsByTags(sexActors.length, "Oral")

(GetOwningQuest() as DSC_SaarthalCollarQuest).RegisterForModEvent("AnimationEnd_ArchmageSexEnd", "ArchmageSexDone")
SexLab.StartSex(sexActors, anims, hook = "ArchmageSexEnd")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

Actor Property SavosRef  Auto  

SexLabFramework Property SexLab  Auto  
