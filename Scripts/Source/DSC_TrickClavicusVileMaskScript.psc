Scriptname DSC_TrickClavicusVileMaskScript extends ObjectReference  

Armor Property DSC_TrickClavicusVileMask Auto
DSC_Utility Property util Auto
DSC_MCM Property config Auto
GlobalVariable Property TimeScale auto
GlobalVariable Property DSC_ClavicusVileTrickEquip Auto
DSC_ClavicusVileMaskQuestScript Property QuestScript Auto

Event OnEquipped(Actor akActor)
  If akActor == Game.GetPlayer()
    DSC_ClavicusVileTrickEquip.SetValueInt(True as Int)
    If util.libs.ForceEquipDevice(akActor, util.DSC_ClavicusVileMaskInventory, util.DSC_ClavicusVileMaskRendered, util.libs.zad_DeviousHood)
      akActor.RemoveItem(DSC_TrickClavicusVileMask, abSilent = True)

      Debug.Messagebox("As you put on the mask, you suddenly hear Clavicus Vile's voice again:")
      Utility.Wait(0.1)
      Debug.Messagebox("'What a fool you are. I'm a god. How can you trick a god? What a grand and intoxicating innocence! There is no escape, no key or spell can work on this mask.'")
      Utility.Wait(0.1)
      Debug.Messagebox("Well, you could maybe convince someone to help you remove it, though good luck, it's enchanted to prevent that.'")
      Utility.Wait(0.1)
      Debug.Messagebox("'Though I may have forgotten to make that bit permanent... Well, just forget you heard that last bit, mortal!'")
      Utility.Wait(0.1)
      Debug.Messagebox("'Now, I have to be off, deals to make, dumb sluts to trick!'")
    Else
      Debug.Messagebox("Can't equip the mask while a quest hood is already equipped")
    EndIf
  EndIf
EndEvent
