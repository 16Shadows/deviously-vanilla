Scriptname MG02AmuletTriggerScript extends ReferenceAlias  

ObjectReference Property SpikeTrigger  Auto  

ReferenceAlias Property MG02WallAlias  Auto
Quest Property MG02  Auto


bool DoOnce = False




Event OnActivate(objectreference Actionref)

	if ActionRef == Game.GetPlayer()
		if (MG02.IsStageDone(30)) ;This prevents that the player rushes in and triggers the amulet too early. (forward from Skyrim Unlocked)
		  If DoOnce == False
			  DoOnce = True
			  MG02WallAlias.GetReference().PlayAnimation("Take")
        ; Game.GetPlayer().AddItem(MG02AmuletAlias.GetReference(), 1)
			  Game.GetPlayer().AddItem(DSC_MG02TrickAmulet, 1)
			  MG02TrapCollision01Ref.Enable()
			  SpikeTrigger.Activate(Game.GetPlayer())
			  MG02.SetStage(40)

			  if MG02.IsObjectiveDisplayed(30)
			    (MG02 as MG02QuestScript).VCount()
			  endif

			  Self.GetReference().Disable()
		  endif
	  endif
	endif



EndEvent
Armor Property DSC_MG02TrickAmulet  Auto  

ReferenceAlias Property MG02AmuletAlias  Auto  

ObjectReference Property MG02TrapCollision01Ref  Auto  
