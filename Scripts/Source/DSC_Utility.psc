Scriptname DSC_Utility extends Quest

import PapyrusUtil
import StorageUtil

DSC_MCM Property config  Auto
DSC_FollowerDetector Property followers Auto

;; DDi/x libraries
zadlibs Property libs  Auto
zadxLibs Property libsx  Auto
zadxLibs2 Property libsx2  Auto

;; MQ106 quest for sigterm
Quest Property DSC_MQ106 Auto
Actor Property PlayerRef Auto

;; Device Formlists
FormList Property DSC_FreeDeviceSlots   Auto
FormList Property DSC_DeviceKeywords    Auto
FormList Property DSC_SuppressedDevices Auto

;; Temp formlists for unequipping devices then reequipping them (eg removing gags for a dialogue)
FormList Property DSC_TemporaryDevices          Auto
FormList Property DSC_TemporaryDevicesRendered  Auto
FormList Property DSC_TemporaryDevicesKeywords  Auto

;; Formlists containing available devices
FormList Property DSC_RandomArmbinderList                         Auto
FormList Property DSC_RandomArmbinderRenderedList                 Auto
FormList Property DSC_RandomArmCuffsList                          Auto
FormList Property DSC_RandomArmCuffsRenderedList                  Auto
FormList Property DSC_RandomBeltList                              Auto
FormList Property DSC_RandomBeltRenderedList                      Auto
FormList Property DSC_RandomBlindfoldList                         Auto
FormList Property DSC_RandomBlindfoldRenderedList                 Auto
FormList Property DSC_RandomBootsList                             Auto
FormList Property DSC_RandomBootsRenderedList                     Auto
FormList Property DSC_RandomBraList                               Auto
FormList Property DSC_RandomBraRenderedList                       Auto
FormList Property DSC_RandomCatsuitsList                          Auto
FormList Property DSC_RandomCatsuitsRenderedList                  Auto
FormList Property DSC_RandomCollarList                            Auto
FormList Property DSC_RandomCollarRenderedList                    Auto
FormList Property DSC_RandomCorsetList                            Auto
FormList Property DSC_RandomCorsetRenderedList                    Auto
FormList Property DSC_RandomGagBallList                           Auto
FormList Property DSC_RandomGagBallRenderedList                   Auto
FormList Property DSC_RandomGagPanelList                          Auto
FormList Property DSC_RandomGagPanelRenderedList                  Auto
FormList Property DSC_RandomGagRingList                           Auto
FormList Property DSC_RandomGagRingRenderedList                   Auto
FormList Property DSC_RandomGlovesList                            Auto
FormList Property DSC_RandomGlovesRenderedList                    Auto
FormList Property DSC_RandomHarnessList                           Auto
FormList Property DSC_RandomHarnessRenderedList                   Auto
FormList Property DSC_RandomHobbleskirtList                       Auto
FormList Property DSC_RandomHobbleskirtRenderedList               Auto
FormList Property DSC_RandomHobbleskirtStrictList                 Auto
FormList Property DSC_RandomHobbleskirtStrictRenderedList         Auto
FormList Property DSC_RandomHoodList                              Auto
FormList Property DSC_RandomHoodRenderedList                      Auto
FormList Property DSC_RandomHoodGagList                           Auto
FormList Property DSC_RandomHoodGagRenderedList                   Auto
FormList Property DSC_RandomHoodGagBlindList                      Auto
FormList Property DSC_RandomHoodGagBlindRenderedList              Auto
FormList Property DSC_RandomLegCuffsList                          Auto
FormList Property DSC_RandomLegCuffsRenderedList                  Auto
FormList Property DSC_RandomMittensList                           Auto
FormList Property DSC_RandomMittensRenderedList                   Auto
FormList Property DSC_RandomPiercingsClitList                     Auto
FormList Property DSC_RandomPiercingsClitRenderedList             Auto
FormList Property DSC_RandomPiercingsNippleList                   Auto
FormList Property DSC_RandomPiercingsNippleRenderedList           Auto
FormList Property DSC_RandomPlugAnalList                          Auto
FormList Property DSC_RandomPlugAnalRenderedList                  Auto
FormList Property DSC_RandomPlugVaginalList                       Auto
FormList Property DSC_RandomPlugVaginalRenderedList               Auto
FormList Property DSC_RandomStraitjacketHobbleList                Auto
FormList Property DSC_RandomStraitjacketHobbleRenderedList        Auto
FormList Property DSC_RandomStraitjacketHobbleStrictList          Auto
FormList Property DSC_RandomStraitjacketHobbleStrictRenderedList  Auto
FormList Property DSC_RandomStraitjacketList                      Auto
FormList Property DSC_RandomStraitjacketRenderedList              Auto

;; Custom keyword for random device picking.
Keyword Property DSC_zad_DeviousHoodOpen Auto

;; Custom devices used
Armor Property FitForAJarlElegant                    Auto
Armor Property FitForAJarlElegantRendered            Auto
Armor Property FitForAJarlRelaxed                    Auto
Armor Property FitForAJarlRelaxedRendered            Auto
Armor Property FitForAJarlStrict                     Auto
Armor Property FitForAJarlStrictRendered             Auto
Armor Property FitForAJarlStraitjacket               Auto
Armor Property FitForAJarlStraitjacketRendered       Auto
Armor Property DSC_MQ106RingGagInventory             Auto
Armor Property DSC_MQ106RingGagRendered              Auto
Armor Property DSC_MQ106CuffsInventory               Auto
Armor Property DSC_MQ106CuffsRendered                Auto
Armor Property DSC_AdrianneArmCuffsInventory         Auto
Armor Property DSC_AdrianneArmCuffsRendered          Auto
Armor Property DSC_AdrianneLegCuffsInventory         Auto
Armor Property DSC_AdrianneLegCuffsRendered          Auto
Armor Property DSC_AdrianneCollarInventory           Auto
Armor Property DSC_AdrianneCollarRendered            Auto
Armor Property DSC_UlfberthArmbinderInventory        Auto
Armor Property DSC_UlfberthArmbinderRendered         Auto
Armor Property DSC_UlfberthArmbinderLooseInventory   Auto
Armor Property DSC_UlfberthArmbinderLooseRendered    Auto
Armor Property DSC_BlackSoulgemVagInventory          Auto
Armor Property DSC_BlackSoulgemVagRendered           Auto
Armor Property DSC_BlackSoulgemAnInventory           Auto
Armor Property DSC_BlackSoulgemAnRendered            Auto
Armor Property DSC_ClavicusVileMaskInventory         Auto
Armor Property DSC_ClavicusVileMaskRendered          Auto
Armor Property DSC_ClavicusVileMaskBInventory        Auto
Armor Property DSC_ClavicusVileMaskBRendered         Auto
Armor Property DSC_ClavicusVileMaskCInventory        Auto
Armor Property DSC_ClavicusVileMaskCRendered         Auto
Armor Property DSC_ClavicusVileMaskGInventory        Auto
Armor Property DSC_ClavicusVileMaskGRendered         Auto
Armor Property DSC_ClavicusVileMaskBCInventory       Auto
Armor Property DSC_ClavicusVileMaskBCRendered        Auto
Armor Property DSC_ClavicusVileMaskBGInventory       Auto
Armor Property DSC_ClavicusVileMaskBGRendered        Auto
Armor Property DSC_ClavicusVileMaskCGInventory       Auto
Armor Property DSC_ClavicusVileMaskCGRendered        Auto
Armor Property DSC_ClavicusVileMaskBCGInventory      Auto
Armor Property DSC_ClavicusVileMaskBCGRendered       Auto
Armor Property DSC_TrickClavicusVileMask             Auto
Armor Property DSC_LodCollarInventory				 Auto
Armor Property DSC_LodCollarRendered				 Auto
Armor Property DSC_RopeArmBindsInventory			 Auto
Armor Property DSC_RopeArmBindsRendered				 Auto


; Quest Devices
Armor Property DSC_SpecialDeliveryPlugAnalInventory	 Auto
Armor Property DSC_SpecialDeliveryPlugAnalRendered	 Auto
Armor Property DSC_SpecialDeliveryPlugVaginalInventory	Auto
Armor Property DSC_SpecialDeliveryPlugVaginalRendered	Auto
Keyword Property DSC_SpecialDeliveryPlugKW Auto
Armor Property DSC_SpecialDeliveryBeltInventory		 Auto
Armor Property DSC_SpecialDeliveryBeltRendered		 Auto
Keyword Property DSC_SpecialDeliveryBeltKW Auto
Armor Property DSC_LodPetsuitInventory				 Auto
Armor Property DSC_LodPetsuitRendered				 Auto
Keyword Property DSC_LodPetsuitKW Auto
Armor Property DSC_SaarthalCollarInventory           Auto
Armor Property DSC_SaarthalCollarRendered            Auto
Keyword Property DSC_SaarthalCollarKW Auto

;

Function OnGameReload()
	OnInit()
EndFunction

Event OnInit()
  RegisterForModEvent("DDi_Quest_SIGTERM", "DDi_MQ106_SIGTERM")
  RegisterForModEvent("DSC_Quest_SIGTERM", "DSC_MQ106_SIGTERM")

  RegisterForModEvent("DSC_AsyncVibrateEffect", "AsyncVibrateEffect")
  RegisterForModEvent("DSC_AsyncActorOrgasm", "AsyncActorOrgasm")

  If libsx2 == None
    ;; One version didn't have this filled, fix for that.
    libsx2 = Quest.GetQuest("zadxQuest") as zadxLibs2
  EndIf

  If PlayerRef == None
    ;; One version didn't have this filled, fix for that.
    PlayerRef = Game.GetPlayer()
  EndIf
  
  If Math.LogicalAnd(DSC_RandomStraitjacketHobbleStrictList.GetFormID(), 0x00FFFFFF) != 0x07E6D4
  	Debug.Notification("Fixing formlist mismatch")
	Formlist temp = DSC_RandomStraitjacketHobbleStrictList
	DSC_RandomStraitjacketHobbleStrictList = DSC_RandomStraitjacketHobbleStrictRenderedList
	DSC_RandomStraitjacketHobbleStrictRenderedList = temp
  EndIf
EndEvent

;; (Un)equips a device based on Inventory armor.
;; Used to (un)equip specific custom devices or devices that aren't in ManipulateDevice of DDi/DDx.
Function ManipulateDevice(Actor akActor, Armor device, bool skipEvents = false, bool force = false, bool unequip = false, bool destroyDevice = false)
  Armor deviceRendered
  Keyword deviceKeyword
  Keyword questDeviceKW

  If device == FitForAJarlElegant
    deviceRendered = FitForAJarlElegantRendered
    deviceKeyword = libs.zad_DeviousHobbleSkirtRelaxed
  ElseIf device == FitForAJarlRelaxed
    deviceRendered = FitForAJarlRelaxedRendered
    deviceKeyword = libs.zad_DeviousHobbleSkirtRelaxed
  ElseIf device == FitForAJarlStrict
    deviceRendered = FitForAJarlStrictRendered
    deviceKeyword = libs.zad_DeviousHobbleSkirt
  ElseIf device == FitForAJarlStraitjacket
    deviceRendered = FitForAJarlStraitjacketRendered
    deviceKeyword = libs.zad_DeviousStraitJacket
  ElseIf device == libsx.zadx_Armbinder_Rope_Inventory
    deviceRendered = libsx.zadx_Armbinder_Rope_Rendered
    deviceKeyword = libs.zad_DeviousArmbinder
  ElseIf device == libsx.zadx_hood_leather_black_Inventory
    deviceRendered = libsx.zadx_hood_leather_black_Rendered
    deviceKeyword = libs.zad_DeviousHood
  ElseIf device == DSC_SaarthalCollarInventory
    deviceRendered = DSC_SaarthalCollarRendered
    deviceKeyword = libs.zad_DeviousCollar
	questDeviceKW = DSC_SaarthalCollarKW
  ElseIf device == DSC_MQ106RingGagInventory
    deviceRendered = DSC_MQ106RingGagRendered
    deviceKeyword = libs.zad_DeviousGag
  ElseIf device == DSC_MQ106CuffsInventory
    deviceRendered = DSC_MQ106CuffsRendered
    deviceKeyword = libs.zad_DeviousHeavyBondage
  ElseIf device == libsx2.zadx_Collar_Puppy_Inventory
    deviceRendered = libsx2.zadx_Collar_Puppy_Rendered
    deviceKeyword = libs.zad_DeviousCollar
  ElseIf device == DSC_AdrianneArmCuffsInventory
    deviceRendered = DSC_AdrianneArmCuffsRendered
    deviceKeyword = libs.zad_DeviousArmCuffs
  ElseIf device == DSC_AdrianneLegCuffsInventory
    deviceRendered = DSC_AdrianneLegCuffsRendered
    deviceKeyword = libs.zad_DeviousLegCuffs
  ElseIf device == DSC_AdrianneCollarInventory
    deviceRendered = DSC_AdrianneCollarRendered
    deviceKeyword = libs.zad_DeviousCollar
  ElseIf device == DSC_UlfberthArmbinderInventory
    deviceRendered = DSC_UlfberthArmbinderRendered
    deviceKeyword = libs.zad_DeviousHeavyBondage
  ElseIf device == DSC_BlackSoulgemVagInventory
    deviceRendered = DSC_BlackSoulgemVagRendered
    deviceKeyword = libs.zad_DeviousPlugVaginal
  ElseIf device == DSC_BlackSoulgemAnInventory
    deviceRendered = DSC_BlackSoulgemAnRendered
    deviceKeyword = libs.zad_DeviousPlugAnal
  ElseIf device == DSC_LodCollarInventory
	deviceRendered = DSC_LodCollarRendered
	deviceKeyword = libs.zad_DeviousCollar
  ElseIf device == DSC_LodPetsuitInventory
	deviceRendered = DSC_LodPetsuitRendered
	deviceKeyword = libs.zad_DeviousPetSuit
	questDeviceKW = DSC_LodPetsuitKW
  ElseIf device == DSC_RopeArmBindsInventory
	deviceRendered = DSC_RopeArmBindsRendered
	deviceKeyword = libs.zad_DeviousHeavyBondage
  ElseIf device == DSC_SpecialDeliveryPlugAnalInventory
	deviceRendered = DSC_SpecialDeliveryPlugAnalRendered
	deviceKeyword = libs.zad_DeviousPlugAnal
	questDeviceKW = DSC_SpecialDeliveryPlugKW
  ElseIf device == DSC_SpecialDeliveryPlugVaginalInventory
	deviceRendered = DSC_SpecialDeliveryPlugVaginalRendered
	deviceKeyword = libs.zad_DeviousPlugVaginal
	questDeviceKW = DSC_SpecialDeliveryPlugKW
  ElseIf device == DSC_SpecialDeliveryBeltInventory
	deviceRendered = DSC_SpecialDeliveryBeltRendered
	deviceKeyword = libs.zad_DeviousBelt
	questDeviceKW = DSC_SpecialDeliveryBeltKW
  EndIf

  If unequip && akActor.IsEquipped(deviceRendered)
	If device.HasKeyword(libs.zad_QuestItem)
		libs.RemoveQuestDevice(akActor, device, deviceRendered, deviceKeyword, questDeviceKW, destroyDevice = destroyDevice)
	Else
		libs.RemoveDevice(akActor, device, deviceRendered, deviceKeyword, destroyDevice = destroyDevice, skipEvents = skipEvents)
	Endif
  ElseIf unequip
    Debug.Trace("DSC: Device is not equipped; skipping unequip")
  Else
    If force
      libs.ForceEquipDevice(akActor, device, deviceRendered, deviceKeyword, skipEvents = skipEvents)
    ElseIf !libs.GetWornDevice(akActor, deviceKeyword)
      libs.EquipDevice(akActor, device, deviceRendered, deviceKeyword, skipEvents = skipEvents)
    Else
      Debug.Trace("DSC: Actor already has device worn in that slot; skipping equip.")
    EndIf
  EndIf
EndFunction

;; Legacy function, just here for scripts that used the old version
;; Equips a device based on Inventory armor.
;; Used to equip specific custom devices or devices that aren't in ManipulateDevice of DDi/DDx.
Function EquipDevice(Actor akActor, Armor device, bool skipEvents = false, bool force = false)
  ManipulateDevice(akActor, device, skipEvents = skipEvents, force = force, unequip = false)
EndFunction

;;;; Functions to equip random device to an actor.
;; Generic function to get a random device from a supplied list.
;; Formlists are index matched, and contain inventory and rendered devices in the same order.
Bool Function EquipRandomDeviceFromList(Actor akActor, Formlist InventoryList, Formlist RenderedList, Keyword kw)
  If !akActor.WornHasKeyword(kw)
    Int i = Utility.RandomInt(0, InventoryList.GetSize() - 1 )
    libs.EquipDevice(akActor, InventoryList.GetAt(i) as Armor, RenderedList.GetAt(i) as Armor, kw)
    return True
  EndIf
  return False
EndFunction

Bool Function EquipRandomArmbinder(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomArmbinderList, DSC_RandomArmbinderRenderedList, libs.zad_DeviousHeavyBondage)
EndFunction

Bool Function EquipRandomArmCuffs(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomArmCuffsList, DSC_RandomArmCuffsRenderedList, libs.zad_DeviousArmCuffs)
EndFunction

Bool Function EquipRandomAnalPlug(Actor akActor)
  If !akActor.WornHasKeyword(libs.zad_DeviousBelt)
    return EquipRandomDeviceFromList(akActor, DSC_RandomPlugAnalList, DSC_RandomPlugAnalRenderedList, libs.zad_DeviousPlugAnal)
  EndIf
  return False
EndFunction

Bool Function EquipRandomVaginalPlug(Actor akActor)
  If !akActor.WornHasKeyword(libs.zad_DeviousBelt)
    return EquipRandomDeviceFromList(akActor, DSC_RandomPlugVaginalList, DSC_RandomPlugVaginalRenderedList, libs.zad_DeviousPlugVaginal)
  EndIf
  return False
EndFunction

Function EquipRandomPlugs(Actor akActor)
  EquipRandomAnalPlug(akActor)
  EquipRandomVaginalPlug(akActor)
EndFunction

Bool Function EquipRandomBelt(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomBeltList, DSC_RandomBeltRenderedList, libs.zad_DeviousBelt)
EndFunction

Bool Function EquipRandomBlindfold(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomBlindfoldList, DSC_RandomBlindfoldRenderedList, libs.zad_DeviousBlindfold)
EndFunction

Bool Function EquipRandomBoots(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomBootsList, DSC_RandomBootsRenderedList, libs.zad_DeviousBoots)
EndFunction

Bool Function EquipRandomBra(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomBraList, DSC_RandomBraRenderedList, libs.zad_DeviousBra)
EndFunction

Bool Function EquipRandomCatsuit(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomCatsuitsList, DSC_RandomCatsuitsRenderedList, libs.zad_DeviousSuit)
EndFunction

Bool Function EquipRandomCollar(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomCollarList, DSC_RandomCollarRenderedList, libs.zad_DeviousCollar)
EndFunction

Bool Function EquipRandomCorset(Actor akActor)
  If !akActor.WornHasKeyword(libs.zad_DeviousHarness)
    return EquipRandomDeviceFromList(akActor, DSC_RandomCorsetList, DSC_RandomCorsetRenderedList, libs.zad_DeviousCorset)
  EndIf
  return False
EndFunction

Bool Function EquipRandomGagBall(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomGagBallList, DSC_RandomGagBallRenderedList, libs.zad_DeviousGag)
EndFunction

Bool Function EquipRandomGagPanel(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomGagPanelList, DSC_RandomGagPanelRenderedList, libs.zad_DeviousGag)
EndFunction

Bool Function EquipRandomGagRing(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomGagRingList, DSC_RandomGagRingRenderedList, libs.zad_DeviousGag)
EndFunction

Bool Function EquipRandomGag(Actor akActor)
  ;; Probability of certain type of gag is based on amount of that type to avoid bias towards specific gags
  ;; This does create a bias towards ball gags, however
  Int amountBalls = DSC_RandomGagBallList.GetSize()
  int amountPanels = DSC_RandomGagPanelList.GetSize()
  Int amountRings = DSC_RandomGagRingList.GetSize()

  Int totalGags = amountBalls + amountPanels + amountRings
  Int r = Utility.RandomInt(0, totalGags)
  If r < amountBalls
    return EquipRandomGagBall(akActor)
  ElseIf r - amountBalls < amountPanels
    return EquipRandomGagPanel(akActor)
  ElseIf r - amountBalls - amountPanels < amountRings
    return EquipRandomGagRing(akActor)
  EndIf
EndFunction

Bool Function EquipRandomGloves(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomGlovesList, DSC_RandomGlovesRenderedList, libs.zad_DeviousGloves)
EndFunction

Bool Function EquipRandomHarness(Actor akActor)
  ;; I don't believe there are any harnesses that have no collar or belt.
  If !akActor.WornHasKeyword(libs.zad_DeviousCorset) && !akActor.WornHasKeyword(libs.zad_DeviousCollar) && !akActor.WornHasKeyword(libs.zad_DeviousBelt)
    return EquipRandomDeviceFromList(akActor, DSC_RandomHarnessList, DSC_RandomHarnessRenderedList, libs.zad_DeviousHarness)
  EndIf
  return False
EndFunction

Bool Function EquipRandomHobbleskirt(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomHobbleskirtList, DSC_RandomHobbleskirtRenderedList, libs.zad_DeviousSuit)
EndFunction

Bool Function EquipRandomHobbleskirtStrict(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomHobbleskirtStrictList, DSC_RandomHobbleskirtStrictRenderedList, libs.zad_DeviousSuit)
EndFunction

Bool Function EquipRandomLegCuffs(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomLegCuffsList, DSC_RandomLegCuffsRenderedList, libs.zad_DeviousLegCuffs)
EndFunction

Bool Function EquipRandomMittens(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomMittensList, DSC_RandomMittensRenderedList, libs.zad_DeviousGloves)
EndFunction

Bool Function EquipRandomPiercingsClit(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomPiercingsClitList, DSC_RandomPiercingsClitRenderedList, libs.zad_DeviousPiercingsVaginal)
EndFunction

Bool Function EquipRandomPiercingsNipple(Actor akActor)
  return EquipRandomDeviceFromList(akActor, DSC_RandomPiercingsNippleList, DSC_RandomPiercingsNippleRenderedList, libs.zad_DeviousPiercingsNipple)
EndFunction

Bool Function EquipRandomYoke(Actor akActor)
  If !akActor.WornHasKeyword(libs.zad_DeviousHeavyBondage)
    Int r
    If akActor.WornHasKeyword(libs.zad_DeviousSuit)
      r = Utility.RandomInt(0,1)
    Else
      r = Utility.RandomInt(0,2)
    EndIf

    If r == 0
      libs.EquipDevice(akActor, libs.yoke, libs.yokeRendered, libs.zad_DeviousHeavyBondage)
    ElseIf r == 1
      libs.EquipDevice(akActor, libsx.zadx_yoke_steel_Inventory, libsx.zadx_yoke_steel_Rendered, libs.zad_DeviousHeavyBondage)
    ElseIf r == 2
      libs.EquipDevice(akActor, libsx.zadx_HR_BBYokeInventory, libsx.zadx_HR_BBYokeRendered, libs.zad_DeviousHeavyBondage)
    EndIf
    return True
  EndIf
  return False
EndFunction

; Bool Function EquipRandomWristCuffs(Actor akActor)
;   return EquipRandomDeviceFromList(akActor, DSC_RandomWristCuffsList, DSC_RandomWristCuffsRenderedList, libs.zad_DeviousHeavyBondage)
; EndFunction

Bool Function EquipRandomStraitJacket(Actor akActor)
  If !akActor.WornHasKeyword(libs.zad_DeviousSuit)
    return EquipRandomDeviceFromList(akActor, DSC_RandomStraitjacketList, DSC_RandomStraitjacketRenderedList, libs.zad_DeviousHeavyBondage)
  EndIf
  return False
EndFunction

Bool Function EquipRandomStraitHobble(Actor akActor)
  If !akActor.WornHasKeyword(libs.zad_DeviousSuit) && !akActor.WornHasKeyword(libs.zad_DeviousHobbleSkirt)
    return EquipRandomDeviceFromList(akActor, DSC_RandomStraitjacketHobbleList, DSC_RandomStraitjacketHobbleRenderedList, libs.zad_DeviousHeavyBondage)
  EndIf
  return False
EndFunction

Bool Function EquipRandomStraitHobbleStrict(Actor akActor)
  If !akActor.WornHasKeyword(libs.zad_DeviousSuit) && !akActor.WornHasKeyword(libs.zad_DeviousHobbleSkirt)
    return EquipRandomDeviceFromList(akActor, DSC_RandomStraitjacketHobbleStrictList, DSC_RandomStraitjacketHobbleStrictRenderedList, libs.zad_DeviousHeavyBondage)
  EndIf
  return False
EndFunction

Bool Function EquipRandomHood(Actor akActor, Bool blindfold = True, Bool gag = True)
  ;; Check if we can use blindfolds or gags
  Bool UseBlindfold = !akActor.WornHasKeyword(libs.zad_DeviousBlindfold) && blindfold
  Bool UseGag = !akActor.WornHasKeyword(libs.zad_DeviousGag) && gag

  If UseBlindfold && UseGag
    return EquipRandomDeviceFromList(akActor, DSC_RandomHoodGagBlindList, DSC_RandomHoodGagBlindRenderedList, libs.zad_DeviousHood)
  ElseIf UseGag
    return EquipRandomDeviceFromList(akActor, DSC_RandomHoodGagList, DSC_RandomHoodGagRenderedList, libs.zad_DeviousHood)
  ElseIf UseBlindfold
    ; return EquipRandomDeviceFromList(akActor, DSC_RandomHoodBlindList, DSC_RandomHoodBlindRenderedList, libs.zad_DeviousHood)
    ;; DDi and DDx have no hoods with only a blindfold, might need changing if adding devices from a mod that does.
    Debug.Trace("DSC: EquipRandomHood; Blindfolded hood needs gag available, using regular hood instead")
    return EquipRandomDeviceFromList(akActor, DSC_RandomHoodList, DSC_RandomHoodRenderedList, libs.zad_DeviousHood)
  Else
    return EquipRandomDeviceFromList(akActor, DSC_RandomHoodList, DSC_RandomHoodRenderedList, libs.zad_DeviousHood)
  EndIf
EndFunction

Bool Function EquipRandomHeavyBondage(Actor akActor, Bool hobble = True, Bool strictHobble = True)
  Int amountArmbinders = DSC_RandomArmbinderList.GetSize()

  ;; Straitjackets can only be equipped when no suit is equipped, same as breast yoke.
  Int amountYokes = 2
  Int amountStraitjackets = 0
  Int amountStraitjacketsHobble = 0
  Int amountStraitjacketsHobbleStrict = 0

  If !akActor.WornHasKeyword(libs.zad_DeviousSuit)
    amountYokes = 3
    amountStraitjackets = DSC_RandomStraitjacketList.GetSize()
    If hobble
      amountStraitjacketsHobble = DSC_RandomStraitjacketHobbleList.GetSize()
      If strictHobble
        amountStraitjacketsHobbleStrict = DSC_RandomStraitjacketHobbleStrictList.GetSize()
      EndIf
    EndIf
  EndIf

  Int totalDevices = amountArmbinders + amountYokes + amountStraitjackets + amountStraitjacketsHobble + amountStraitjacketsHobbleStrict
  Int r = Utility.RandomInt(0, totalDevices)

  If r < amountArmbinders
    return EquipRandomArmbinder(akActor)
  EndIf
  r -= amountArmbinders
  If r < amountYokes
    return EquipRandomYoke(akActor)
  EndIf
  r -= amountYokes
  If r < amountStraitjackets
    return EquipRandomStraitJacket(akActor)
  EndIf
  r -= amountStraitjackets
  If r < amountStraitjacketsHobble
    return EquipRandomStraitHobble(akActor)
  EndIf
  r -= amountStraitjacketsHobble
  If r < amountStraitjacketsHobbleStrict
    return EquipRandomStraitHobbleStrict(akActor)
  EndIf
EndFunction

Keyword Function EquipRandomDevice(Actor akActor, Bool ownCustom = False)
  If !ownCustom
    FillSuppressed(UseHeavy = config.GeneralUseHeavy, UseChastity = config.GeneralUseChastity, UseGag = config.GeneralUseGag, UseBlindfold = config.GeneralUseBlindfold, UseMittens = config.GeneralUseMittens, useHobble = config.GeneralUseHobble, UseStrictHobble = config.GeneralUseStrictHobble)
  EndIf

  Int i
  Keyword kw

  i = DSC_DeviceKeywords.GetSize() - 1
  DSC_FreeDeviceSlots.Revert()
  While i >= 0
    kw = DSC_DeviceKeywords.GetAt(i) as Keyword
    ;; If actor is already wearing device or if device is suppressed, don't use that slot.
    If !akActor.WornHasKeyword(kw) && DSC_SuppressedDevices.Find(kw) == -1
      If (kw == libs.zad_DeviousHarness || kw == libs.zad_DeviousCorset) && (akActor.WornHasKeyword(libs.zad_DeviousHarness) || akActor.WornHasKeyword(libs.zad_DeviousCorset))
        ;; Don't add harness or corset if either is worn.
      Else
        DSC_FreeDeviceSlots.AddForm(kw)
      EndIf
    EndIf
    i -= 1
  EndWhile

  If DSC_FreeDeviceSlots.GetSize() == 0
    Debug.Trace("DSC: No available device slots")
    return None
  EndIf

  i = Utility.RandomInt(0, DSC_FreeDeviceSlots.GetSize() - 1)
  kw = DSC_FreeDeviceSlots.GetAt(i) as Keyword
  If kw == libs.zad_DeviousArmCuffs
    EquipRandomArmCuffs(akActor)
  ElseIf kw == libs.zad_DeviousBelt
    EquipRandomBelt(akActor)
  ElseIf kw == libs.zad_DeviousBlindfold
    EquipRandomBlindfold(akActor)
  ElseIf kw == libs.zad_DeviousBoots
    EquipRandomBoots(akActor)
  ElseIf kw == libs.zad_DeviousBra
    EquipRandomBra(akActor)
  ElseIf kw == libs.zad_DeviousCollar
    EquipRandomCollar(akActor)
  ElseIf kw == libs.zad_DeviousCorset
    EquipRandomCorset(akActor)
  ElseIf kw == libs.zad_DeviousGag
    EquipRandomGag(akActor)
  ElseIf kw == libs.zad_DeviousGloves
    EquipRandomGloves(akActor)
  ElseIf kw == libs.zad_DeviousHarness
    EquipRandomHarness(akActor)
  ElseIf kw == libs.zad_DeviousHood
    EquipRandomHood(akActor)
  ElseIf kw == DSC_zad_DeviousHoodOpen
    EquipRandomHood(akActor, False, False)
  ElseIf kw == libs.zad_DeviousLegCuffs
    EquipRandomLegCuffs(akActor)
  ElseIf kw == libs.zad_DeviousPiercingsNipple
    EquipRandomPiercingsNipple(akActor)
  ElseIf kw == libs.zad_DeviousPiercingsVaginal
    EquipRandomPiercingsClit(akActor)
  ElseIf kw == libs.zad_DeviousPlug
    EquipRandomPlugs(akActor)
  ElseIf kw == libs.zad_DeviousSuit
    EquipRandomCatsuit(akActor)
  ElseIf kw == libs.zad_DeviousHeavyBondage
    If !DSC_SuppressedDevices.HasForm(libs.zad_DeviousHobbleSkirt)
      EquipRandomHeavyBondage(akActor, hobble = True, strictHobble = True)
    ElseIf !DSC_SuppressedDevices.HasForm(libs.zad_DeviousHobbleSkirtRelaxed)
      EquipRandomHeavyBondage(akActor, hobble = True, strictHobble = False)
    Else
      EquipRandomHeavyBondage(akActor, hobble = False, strictHobble = False)
    EndIf
  ElseIf kw == libs.zad_DeviousBondageMittens
    EquipRandomMittens(akActor)
  ElseIf kw == libs.zad_DeviousHobbleSkirt
    EquipRandomHobbleskirtStrict(akActor)
  ElseIf kw == libs.zad_DeviousHobbleSkirtRelaxed
    EquipRandomHobbleskirt(akActor)
  EndIf
  return kw
EndFunction

Function ProgressiveBondage(Actor akActor, Keyword[] Pattern = None, Int Amount = 1)
  If !Pattern
    ;; If no pattern is provided, use this as default.
    Pattern = new Keyword[14]
    Pattern[0]  = libs.zad_DeviousLegCuffs
    Pattern[1]  = libs.zad_DeviousArmCuffs
    Pattern[2]  = libs.zad_DeviousCollar
    Pattern[3]  = libs.zad_DeviousPiercingsNipple
    Pattern[4]  = libs.zad_DeviousPiercingsVaginal
    Pattern[5]  = libs.zad_DeviousBra
    Pattern[6]  = libs.zad_DeviousBelt
    Pattern[7]  = libs.zad_DeviousCorset
    Pattern[8]  = libs.zad_DeviousGloves
    Pattern[9]  = libs.zad_DeviousSuit
    Pattern[10] = libs.zad_DeviousBoots
    Pattern[11] = libs.zad_DeviousGag
    Pattern[12] = libs.zad_DeviousBlindfold
    Pattern[13] = libs.zad_DeviousHeavyBondage
  EndIf

  Int i = 0
  Int j = 0                     ; We can start looking in the pattern at the place we found the last keyword.
  While i < Amount
    Keyword kw

    Bool jBreak = False
    While j < Pattern.length && !jBreak
      ;; Find the first valid keyword in the pattern
      If Pattern[j] != None && !akActor.WornHasKeyword(Pattern[j])
        kw = Pattern[j]
        jBreak = True           ; If found, break the loop and continue with equipping.
      EndIf

      j += 1
    EndWhile

    If kw == None
      ;; No valid keyword was found in the pattern.
      Return
    EndIf

    If kw == libs.zad_DeviousArmCuffs
      EquipRandomArmCuffs(akActor)
    ElseIf kw == libs.zad_DeviousBelt
      EquipRandomBelt(akActor)
    ElseIf kw == libs.zad_DeviousBlindfold
      EquipRandomBlindfold(akActor)
    ElseIf kw == libs.zad_DeviousBoots
      EquipRandomBoots(akActor)
    ElseIf kw == libs.zad_DeviousBra
      EquipRandomBra(akActor)
    ElseIf kw == libs.zad_DeviousCollar
      EquipRandomCollar(akActor)
    ElseIf kw == libs.zad_DeviousCorset
      EquipRandomCorset(akActor)
    ElseIf kw == libs.zad_DeviousGag
      EquipRandomGag(akActor)
    ElseIf kw == libs.zad_DeviousGloves
      EquipRandomGloves(akActor)
    ElseIf kw == libs.zad_DeviousHarness
      EquipRandomHarness(akActor)
    ElseIf kw == libs.zad_DeviousHood
      EquipRandomHood(akActor)
    ElseIf kw == DSC_zad_DeviousHoodOpen
      EquipRandomHood(akActor, False, False)
    ElseIf kw == libs.zad_DeviousLegCuffs
      EquipRandomLegCuffs(akActor)
    ElseIf kw == libs.zad_DeviousPiercingsNipple
      EquipRandomPiercingsNipple(akActor)
    ElseIf kw == libs.zad_DeviousPiercingsVaginal
      EquipRandomPiercingsClit(akActor)
    ElseIf kw == libs.zad_DeviousPlug
      EquipRandomPlugs(akActor)
    ElseIf kw == libs.zad_DeviousSuit
      EquipRandomCatsuit(akActor)
    ElseIf kw == libs.zad_DeviousHeavyBondage
      EquipRandomHeavyBondage(akActor, hobble = False, strictHobble = False)
    ElseIf kw == libs.zad_DeviousBondageMittens
      EquipRandomMittens(akActor)
    ElseIf kw == libs.zad_DeviousHobbleSkirt
      EquipRandomHobbleskirtStrict(akActor)
    ElseIf kw == libs.zad_DeviousHobbleSkirtRelaxed
      EquipRandomHobbleskirt(akActor)
    EndIf

    i += 1
  EndWhile
EndFunction

;; Set devices that we want to suppress.
Function FillSuppressed(Bool UseHeavy = True, Bool UseChastity = True, Bool UseGag = True, Bool UseBlindfold = True, Bool UseMittens = True, Bool useHobble = True, Bool UseStrictHobble = True)
  DSC_SuppressedDevices.Revert()
  If !UseHeavy
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousHeavyBondage)
  EndIf
  If !UseChastity
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousBelt)
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousBra)
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousHarness)
  EndIf
  If !UseGag
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousGag)
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousHood) ; zad_DeviousHood is used only for gag/blindfold hoods
  EndIf
  If !UseBlindfold
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousBlindfold)
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousHood) ; zad_DeviousHood is used only for gag/blindfold hoods
  EndIf
  If !UseMittens
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousBondageMittens)
  EndIf
  If !UseHobble
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousHobbleSkirtRelaxed)
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousHobbleskirt)
  EndIf
  If !UseStrictHobble
    DSC_SuppressedDevices.AddForm(libs.zad_DeviousHobbleskirt)
  EndIf
EndFunction

;; Start vibrate effect asynchronously, so it can be used in fragments.
Event AsyncVibrateEffect(Form akActor, Int VibStrength, Int Duration, Bool TeaseOnly, Bool Silent)
  libs.VibrateEffect(akActor as Actor, VibStrength, Duration, TeaseOnly = TeaseOnly, Silent = Silent)
EndEvent

Function VibrateEffect(Actor akActor, Int VibStrength, Int Duration, Bool TeaseOnly = False, Bool Silent = False)
  If akActor.WornHasKeyword(libs.zad_DeviousPlug)
    Int handle = ModEvent.Create("DSC_AsyncVibrateEffect")
    If handle
      ModEvent.PushForm(handle, akActor as Form)
      ModEvent.PushInt(handle, VibStrength)
      ModEvent.PushInt(handle, Duration)
      ModEvent.PushBool(handle, TeaseOnly)
      ModEvent.PushBool(handle, Silent)
      ModEvent.Send(handle)
    EndIf
  EndIf
EndFunction

;; Start ActorOrgasm asynchronously, so it can be used in fragments.
Event AsyncActorOrgasm(Form akActor)
  libs.ActorOrgasm(akActor as Actor)
EndEvent

Function ActorOrgasm(Actor akActor)
  Int handle = ModEvent.Create("DSC_AsyncActorOrgasm")
  If handle
    ModEvent.PushForm(handle, akActor as Form)
    ModEvent.Send(handle)
  EndIf
EndFunction

;; It seems that a device may get equipped after zadlibs.JamLock gets called in some quests thus the locks don't get jammed
;; So we avoid this case by skipping checking if the device is equipped
bool Function JamLockUnsafe(actor akActor, keyword zad_DeviousDevice)
	If akActor != playerRef
		return False
	Endif
	StorageUtil.SetIntValue(akActor, "zad_Equipped" + libs.LookupDeviceType(zad_DeviousDevice) + "_LockJammedStatus", 1)
	return True
EndFunction

;; We shut down the MQ106 stuff in this script, since it's possible to wear the gag without running DSC_MQ106DelphineUnlock
Event DDi_MQ106_SIGTERM()
  DSC_MQ106.Stop()
EndEvent

Event DSC_MQ106_SIGTERM()
  ManipulateDevice(PlayerRef, DSC_MQ106RingGagInventory, unequip = true, destroyDevice = true)
  ManipulateDevice(PlayerRef, DSC_MQ106CuffsInventory, unequip = true, destroyDevice = true)
  DSC_MQ106.Stop()
EndEvent


;; Check if Actor array has playable race
Bool Function HasPlayableRace(Actor[] actors)
  Int i = actors.length - 1
  While i >= 0
    If I > actors.length
      ;; Used to loop wrong; quit if that has happened on a save.
      Return False
    EndIf
    If actors[i].GetRace().IsPlayable() && actors[i] != PlayerRef
      Return True
    EndIf

    i -= 1
  EndWhile
  Return False
EndFunction

; Semi-proxy functions for followers detector

int Function GetFollowersCount()
	followers.Start()
	int cnt = followers.GetFollowersCount()
	followers.Reset()
	return cnt
EndFunction

Actor[] Function GetFollowersList()
	followers.Start()
	Actor[] actors = followers.GetActorsList()
	followers.Reset()
	return actors
EndFunction


;;;; Legacy functions
;; Selects and equips a random device with keywords to suppress
Keyword Function GetRandomKeyword(Actor akActor)
  ;; Thanks to Kimy in DCL for this straightforward solution I apparently am too stupid to come up with
  Int i
  Keyword kw

  i = DSC_DeviceKeywords.GetSize() - 1
  DSC_FreeDeviceSlots.revert()
  While i >= 0
    kw = DSC_DeviceKeywords.GetAt(i) as Keyword
    If !akActor.WornHasKeyword(kw) && DSC_SuppressedDevices.Find(kw) == -1
      DSC_FreeDeviceSlots.AddForm(kw)
    EndIf
    i -= 1
  EndWhile
  If DSC_FreeDeviceSlots.GetSize() == 0
    return None
  EndIf
  i = Utility.RandomInt(0, DSC_FreeDeviceSlots.GetSize() - 1)
  return DSC_FreeDeviceSlots.GetAt(i) as Keyword
EndFunction

Function EquipRandomDeviceByKeyword(Actor akActor, Keyword kw)
  armor device = libs.GetGenericDeviceByKeyword(kw)
  Armor deviceRendered = libs.GetRenderedDevice(device)
  ; Debug.Trace("KeywordName: " + kw.GetName(), 0)
  ; Debug.Trace("KeywordID: " + kw.GetFormID(), 0)
  ; Debug.Trace("deviceName: " + device.GetName(), 0)
  ; Debug.Trace("deviceID: " + device.GetFormID(), 0)
  libs.EquipDevice(akActor, device, deviceRendered, kw)
EndFunction

Function DumpLists()
  Formlist[] lists = new Formlist[56]
  lists[0]  = DSC_RandomArmbinderList
  lists[1]  = DSC_RandomArmbinderRenderedList
  lists[2]  = DSC_RandomArmCuffsList
  lists[3]  = DSC_RandomArmCuffsRenderedList
  lists[4]  = DSC_RandomBeltList
  lists[5]  = DSC_RandomBeltRenderedList
  lists[6]  = DSC_RandomBlindfoldList
  lists[7]  = DSC_RandomBlindfoldRenderedList
  lists[8]  = DSC_RandomBootsList
  lists[9]  = DSC_RandomBootsRenderedList
  lists[10] = DSC_RandomBraList
  lists[11] = DSC_RandomBraRenderedList
  lists[12] = DSC_RandomCatsuitsList
  lists[13] = DSC_RandomCatsuitsRenderedList
  lists[14] = DSC_RandomCollarList
  lists[15] = DSC_RandomCollarRenderedList
  lists[16] = DSC_RandomCorsetList
  lists[17] = DSC_RandomCorsetRenderedList
  lists[18] = DSC_RandomGagBallList
  lists[19] = DSC_RandomGagBallRenderedList
  lists[20] = DSC_RandomGagPanelList
  lists[21] = DSC_RandomGagPanelRenderedList
  lists[22] = DSC_RandomGagRingList
  lists[23] = DSC_RandomGagRingRenderedList
  lists[24] = DSC_RandomGlovesList
  lists[25] = DSC_RandomGlovesRenderedList
  lists[26] = DSC_RandomHarnessList
  lists[27] = DSC_RandomHarnessRenderedList
  lists[28] = DSC_RandomHobbleskirtList
  lists[29] = DSC_RandomHobbleskirtRenderedList
  lists[30] = DSC_RandomHobbleskirtStrictList
  lists[31] = DSC_RandomHobbleskirtStrictRenderedList
  lists[32] = DSC_RandomHoodList
  lists[33] = DSC_RandomHoodRenderedList
  lists[34] = DSC_RandomHoodGagList
  lists[35] = DSC_RandomHoodGagRenderedList
  lists[36] = DSC_RandomHoodGagBlindList
  lists[37] = DSC_RandomHoodGagBlindRenderedList
  lists[38] = DSC_RandomLegCuffsList
  lists[39] = DSC_RandomLegCuffsRenderedList
  lists[40] = DSC_RandomMittensList
  lists[41] = DSC_RandomMittensRenderedList
  lists[42] = DSC_RandomPiercingsClitList
  lists[43] = DSC_RandomPiercingsClitRenderedList
  lists[44] = DSC_RandomPiercingsNippleList
  lists[45] = DSC_RandomPiercingsNippleRenderedList
  lists[46] = DSC_RandomPlugAnalList
  lists[47] = DSC_RandomPlugAnalRenderedList
  lists[48] = DSC_RandomPlugVaginalList
  lists[49] = DSC_RandomPlugVaginalRenderedList
  lists[50] = DSC_RandomStraitjacketHobbleList
  lists[51] = DSC_RandomStraitjacketHobbleRenderedList
  lists[52] = DSC_RandomStraitjacketHobbleStrictList
  lists[53] = DSC_RandomStraitjacketHobbleStrictRenderedList
  lists[54] = DSC_RandomStraitjacketList
  lists[55] = DSC_RandomStraitjacketRenderedList

  String[] listnames = new String[56]
  listnames[0]  = "DSC_RandomArmbinderList"
  listnames[1]  = "DSC_RandomArmbinderRenderedList"
  listnames[2]  = "DSC_RandomArmCuffsList"
  listnames[3]  = "DSC_RandomArmCuffsRenderedList"
  listnames[4]  = "DSC_RandomBeltList"
  listnames[5]  = "DSC_RandomBeltRenderedList"
  listnames[6]  = "DSC_RandomBlindfoldList"
  listnames[7]  = "DSC_RandomBlindfoldRenderedList"
  listnames[8]  = "DSC_RandomBootsList"
  listnames[9]  = "DSC_RandomBootsRenderedList"
  listnames[10] = "DSC_RandomBraList"
  listnames[11] = "DSC_RandomBraRenderedList"
  listnames[12] = "DSC_RandomCatsuitsList"
  listnames[13] = "DSC_RandomCatsuitsRenderedList"
  listnames[14] = "DSC_RandomCollarList"
  listnames[15] = "DSC_RandomCollarRenderedList"
  listnames[16] = "DSC_RandomCorsetList"
  listnames[17] = "DSC_RandomCorsetRenderedList"
  listnames[18] = "DSC_RandomGagBallList"
  listnames[19] = "DSC_RandomGagBallRenderedList"
  listnames[20] = "DSC_RandomGagPanelList"
  listnames[21] = "DSC_RandomGagPanelRenderedList"
  listnames[22] = "DSC_RandomGagRingList"
  listnames[23] = "DSC_RandomGagRingRenderedList"
  listnames[24] = "DSC_RandomGlovesList"
  listnames[25] = "DSC_RandomGlovesRenderedList"
  listnames[26] = "DSC_RandomHarnessList"
  listnames[27] = "DSC_RandomHarnessRenderedList"
  listnames[28] = "DSC_RandomHobbleskirtList"
  listnames[29] = "DSC_RandomHobbleskirtRenderedList"
  listnames[30] = "DSC_RandomHobbleskirtStrictList"
  listnames[31] = "DSC_RandomHobbleskirtStrictRenderedList"
  listnames[32] = "DSC_RandomHoodList"
  listnames[33] = "DSC_RandomHoodRenderedList"
  listnames[34] = "DSC_RandomHoodGagList"
  listnames[35] = "DSC_RandomHoodGagRenderedList"
  listnames[36] = "DSC_RandomHoodGagBlindList"
  listnames[37] = "DSC_RandomHoodGagBlindRenderedList"
  listnames[38] = "DSC_RandomLegCuffsList"
  listnames[39] = "DSC_RandomLegCuffsRenderedList"
  listnames[40] = "DSC_RandomMittensList"
  listnames[41] = "DSC_RandomMittensRenderedList"
  listnames[42] = "DSC_RandomPiercingsClitList"
  listnames[43] = "DSC_RandomPiercingsClitRenderedList"
  listnames[44] = "DSC_RandomPiercingsNippleList"
  listnames[45] = "DSC_RandomPiercingsNippleRenderedList"
  listnames[46] = "DSC_RandomPlugAnalList"
  listnames[47] = "DSC_RandomPlugAnalRenderedList"
  listnames[48] = "DSC_RandomPlugVaginalList"
  listnames[49] = "DSC_RandomPlugVaginalRenderedList"
  listnames[50] = "DSC_RandomStraitjacketHobbleList"
  listnames[51] = "DSC_RandomStraitjacketHobbleRenderedList"
  listnames[52] = "DSC_RandomStraitjacketHobbleStrictList"
  listnames[53] = "DSC_RandomStraitjacketHobbleStrictRenderedList"
  listnames[54] = "DSC_RandomStraitjacketList"
  listnames[55] = "DSC_RandomStraitjacketRenderedList"

  Int i = 0
  While i < 56
    Debug.Trace("DSC: Formlist Inv Name: " + listnames[i])
    Debug.Trace("DSC: Formlist Ren Name: " + listnames[i + 1])

    Formlist CurrentInvList = lists[i]
    Formlist CurrentRenList = lists[i + 1]
    Int ListLength = CurrentInvList.GetSize()
    Int j = 0

    While j < ListLength
      Form currentInv = CurrentInvList.GetAt(j)
      Form currentRen = CurrentRenList.GetAt(j)

      Debug.Trace("DSC: Inv: " + currentInv.GetFormID() + ": " + currentInv.GetName())
      Debug.Trace("DSC: Ren: " + currentRen.GetFormID())

      j += 1
    EndWhile

    i += 2
  EndWhile
EndFunction
