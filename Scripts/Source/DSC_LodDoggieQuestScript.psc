Scriptname DSC_LodDoggieQuestScript extends Quest  

Scene Property AfterScene  Auto  

int targetStage

Function DelayStage(float delay, int stage)
	targetStage = stage
	RegisterForSingleUpdateGameTime(delay)
EndFunction

Event OnUpdateGameTime()
	SetStage(targetStage)
endEvent

Event OnSceneEnd(string eventName, string argString, float argNum, form sender)
	AfterScene.Start()
EndEvent