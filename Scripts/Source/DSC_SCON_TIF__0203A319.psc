;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_SCON_TIF__0203A319 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
If config.VashaRopeBinder && config.VashaHood
  Debug.MessageBox("Before you can react, Vasha swiftly pulls the hood over your head, and ties your hands together")
  util.EquipDevice(PlayerRef, libsx.zadx_hood_leather_black_Inventory)
  util.EquipDevice(PlayerRef, libsx.zadx_Armbinder_Rope_Inventory)
ElseIf config.VashaRopeBinder
  Debug.MessageBox("Before you can react, Vasha ties your hands behind your back")
  util.EquipDevice(PlayerRef, libsx.zadx_Armbinder_Rope_Inventory)
ElseIf config.VashaHood
  Debug.MessageBox("Before you can react, Vasha swiftly pulls the hood over your head.")
  util.EquipDevice(PlayerRef, libsx.zadx_hood_leather_black_Inventory)
EndIf

Utility.Wait(4.0)
;Start sex scene
Handler.TwoSex(akSpeaker, "Aggressive", 2, 0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

SC_Handler Property Handler  Auto  

zadxLibs Property libsx  Auto  

DSC_MCM Property config  Auto  

DSC_Utility Property Util  Auto  

Actor Property PlayerRef  Auto  
