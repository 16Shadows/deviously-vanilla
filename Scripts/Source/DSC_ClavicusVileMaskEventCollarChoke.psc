Scriptname DSC_ClavicusVileMaskEventCollarChoke Extends zadBaseEvent

Keyword Property DSC_ClavicusCollar Auto

;; @Override
Bool Function HasKeywords(Actor akActor)
  Return akActor.WornHasKeyword(DSC_ClavicusCollar) && akActor == libs.PlayerRef
EndFunction

;; @Override
Function Execute(Actor akActor)
  If akActor != libs.PlayerRef
    Return
  EndIf
  libs.NotifyPlayer("The collar in the mask starts pulling tighter and tighter,")
  libs.NotifyPlayer("until you are completely unable to breathe")

  akActor.GetCombatState()      ; papyrus bad
  If akActor.GetCombatState() != 1
    akActor.DamageActorValue("Stamina", akActor.GetActorValue("Stamina"))
    libs.CatchBreath(akActor)
    akActor.DamageActorValue("Stamina", akActor.GetActorValue("Stamina"))
    libs.NotifyPlayer("Just as you feel your mind slipping away,")
    libs.NotifyPlayer("the collar finally releases its grip on your throat.")
  Else
    akActor.DamageActorValue("Stamina", akActor.GetActorValue("Stamina") * 0.7)
    libs.NotifyPLayer("But luckily it has some mercy, and it quickly releases its grip on your throat.")
  EndIf
EndFunction
