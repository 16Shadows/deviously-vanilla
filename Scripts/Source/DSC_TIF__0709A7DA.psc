;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0709A7DA Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
util.ManipulateDevice(util.PlayerRef, util.DSC_BlackSoulgemAnInventory, unequip = true, destroyDevice = true)
util.ManipulateDevice(util.PlayerRef, util.DSC_BlackSoulgemVagInventory, unequip = true, destroyDevice = true)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property util Auto
