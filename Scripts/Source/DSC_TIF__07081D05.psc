;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__07081D05 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
DSC_MQ106DelphineAfterSexType.SetValueInt(1)

util.ManipulateDevice(PlayerRef, util.DSC_MQ106CuffsInventory, unequip = true, destroyDevice = true)

(GetOwningQuest() as DSC_MQ106DelphineUnlock).RegisterForModEvent("AnimationEnd_DelphineUnlock", "DelphineUnlock")
SexlabUtil.QuickStart(PlayerRef, DelphineRef, victim = PlayerRef, hook = "DelphineUnlock")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

Actor Property DelphineREF  Auto  

GlobalVariable Property DSC_MQ106DelphineAfterSexType  Auto  

DSC_Utility Property Util  Auto  
