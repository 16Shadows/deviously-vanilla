Scriptname DSC_zadPlugScript extends zadPlugScript  

Function RemovePlugNoLock()
  If libs.PlayerRef.WornHasKeyword(libs.zad_DeviousHeavyBondage)
    libs.Notify("You can't take out the plug while your arms are tied", messageBox = true)
    Return
  EndIf
  Parent.RemovePlugNoLock()
EndFunction