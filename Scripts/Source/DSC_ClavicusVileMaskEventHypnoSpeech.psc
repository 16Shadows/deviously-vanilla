Scriptname DSC_ClavicusVileMaskEventHypnoSpeech extends zadBaseEvent

Keyword Property DSC_ClavicusBlindfold Auto
DSC_MCM Property config Auto
ImagespaceModifier Property HypnoImod Auto
Spell Property DSC_ClavicusVileHypnoFortSpeech Auto
GlobalVariable Property DSC_ClavicusVileEventsDone Auto
GlobalVariable Property DSC_ClavicusVileAllowEscape Auto

;; @Override
Bool Function HasKeywords(Actor akActor)
  Return akActor == libs.PlayerRef && akActor.WornHasKeyword(DSC_ClavicusBlindfold)
EndFunction

;; @Override
Function Execute(Actor akActor)
  HypnoImod.Apply(config.CVIModEnabled as Float)
  DSC_ClavicusVileEventsDone.SetValueInt(DSC_ClavicusVileEventsDone.GetValueInt() + 1)
  If DSC_ClavicusVileEventsDone.GetValueInt() > config.CVEventsNeeded
    DSC_ClavicusVileAllowEscape.SetValueInt(True as Int)
  EndIf

  libs.NotifyPlayer("The inside of the masque flashes some strange patterns,")
  libs.NotifyPlayer("instilling a sense of confidence, making it easier to convince others.")
  DSC_ClavicusVileHypnoFortSpeech.Cast(libs.PlayerRef, libs.PlayerRef)
EndFunction
