;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0A0E7D32 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
utils.ManipulateDevice(utils.PlayerRef, utils.DSC_LodPetsuitInventory, unequip = true, destroyDevice = true)

utils.libs.EquipDevice(utils.libs.PlayerRef, utils.libsx2.zadx_PetSuit_Ebonite_Black_Inventory, utils.libsx2.zadx_PetSuit_Ebonite_Black_Rendered, utils.libs.zad_DeviousPetSuit, true)
utils.JamLockUnsafe(utils.libs.PlayerRef, utils.libs.zad_DeviousHeavyBondage)

utils.ManipulateDevice(utils.PlayerRef, utils.DSC_LodCollarInventory, force = true)
;Somehow it doesn't work no matter what. Maybe DD's properties are messed up? Or is it just my skyrim?
;utils.JamLockUnsafe(libs.PlayerRef, libs.zad_DeviousCollar)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property utils  Auto