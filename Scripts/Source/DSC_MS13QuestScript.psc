Scriptname DSC_MS13QuestScript extends Quest  

bool[] controls

Function StartArvelInteraction()
	controls = new bool[8]

	; save controls state
	controls[0] = Game.IsMovementControlsEnabled()
	controls[1] = Game.IsFightingControlsEnabled()
	controls[2] = Game.IsCamSwitchControlsEnabled()
	controls[3] = Game.IsLookingControlsEnabled()
	controls[4] = Game.IsSneakingControlsEnabled()
	controls[5] = Game.IsMenuControlsEnabled()
	controls[6] = Game.IsActivateControlsEnabled()
	controls[7] = Game.IsJournalControlsEnabled()

	LockControls()

	int choice = ArvelAttackMSG.Show()
	if choice==1
		ArvelNoResistScene.Start()
	elseif choice == 0
		ArvelResistMSG.Show()
		utils.PlayerRef.DamageAV("Stamina", utils.PlayerRef.GetAV("Stamina"))
		ArvelResistScene.Start()
	endif
Endfunction

Function LockControls()
	; Disable movement, combat and interaction for the scene
	Game.DisablePlayerControls(true, true, !controls[2], !controls[3], true, !controls[5], true, !controls[7])
endFunction

Function EquipDevices(bool resisted = false)
	; Unequip armor
	utils.PlayerRef.UnequipItemSlot(31)
	utils.PlayerRef.UnequipItemSlot(32)
	utils.PlayerRef.UnequipItemSlot(33)
	utils.PlayerRef.UnequipItemSlot(37)

	ArvelBindMSG.Show()

	If utils.config.GeneralUseHeavy
		utils.libs.EquipDevice(utils.PlayerRef, utils.libsx.zadx_Armbinder_Rope_Inventory, utils.libsx.zadx_Armbinder_Rope_Inventory, utils.libs.zad_DeviousHeavyBondage)
	Else
		utils.ManipulateDevice(utils.PlayerRef, utils.DSC_RopeArmBindsInventory)
	Endif

	; Due to unknown reasons when equipping arm binds via function, they fail to act as an armbinder
	; However, equipping anything else after fixes them, so here we throw in some more devices.
	; Maybe I just messed up the binds and it can be fixed by editing them

	utils.libs.EquipDevice(utils.PlayerRef, utils.libsx.zadx_Collar_Rope_1_Inventory, utils.libsx.zadx_Collar_Rope_1_Rendered, utils.libs.zad_DeviousCollar)

	If resisted
		If utils.config.GeneralUseStrictHobble
			utils.libs.EquipDevice(utils.PlayerRef, utils.libsx.zadx_Harness_Rope_Full_Inventory, utils.libsx.zadx_Harness_Rope_Full_Rendered, utils.libs.zad_DeviousSuit)
		Else
			utils.libs.EquipDevice(utils.PlayerRef, utils.libsx2.zadx_rope_harness_FullTop_Inventory, utils.libsx2.zadx_rope_harness_FullTop_Rendered, utils.libs.zad_DeviousHarness)
		Endif
	
		If utils.config.GeneralUseBlindfold
			utils.libs.EquipDevice(utils.PlayerRef, utils.libsx.zadx_blindfold_Rope_Inventory, utils.libsx.zadx_blindfold_Rope_Rendered, utils.libs.zad_DeviousBlindfold)
		EndIf
	Else	
		utils.libs.EquipDevice(utils.PlayerRef, utils.libsx2.zadx_rope_crotch_Inventory, utils.libsx2.zadx_rope_crotch_Rendered, utils.libs.zad_DeviousBelt)
	Endif

	ArvelDefaultScene.Start()
	Alias_Arvel.GetActorRef().SetGhost(False)

	; Enable controls back
	Game.EnablePlayerControls(controls[0], controls[1], controls[2], controls[3], controls[4], controls[5], controls[6], controls[7])

endFunction

Event ArvelSexNoResist(string eventName, string strArg, float numArg, Form sender)
	ArvelNoResistAfterSexScene.Start()
endEvent

Event ArvelSexResist(string eventName, string strArg, float numArg, Form sender)
	ArvelResistAfterSexScene.Start()
endEvent 

Message Property ArvelAttackMSG  Auto  

Message Property ArvelResistMSG  Auto  

Scene Property ArvelNoResistScene  Auto

Scene Property ArvelResistScene Auto

Scene Property ArvelNoResistAfterSexScene  Auto

Scene Property ArvelResistAfterSexScene Auto

DSC_Utility Property utils  Auto  

Scene Property ArvelDefaultScene  Auto  

Message Property ArvelBindMSG  Auto  

ReferenceAlias Property Alias_Arvel  Auto  
