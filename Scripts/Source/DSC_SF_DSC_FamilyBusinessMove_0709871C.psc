;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 3
Scriptname DSC_SF_DSC_FamilyBusinessMove_0709871C Extends Scene Hidden

;BEGIN FRAGMENT Fragment_2
Function Fragment_2()
;BEGIN CODE
game.disablePlayerControls()
Game.SetPlayerAIDriven(True)

SendModEvent("DHLP-Suspend")
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_1
Function Fragment_1()
;BEGIN CODE
getowningquest().setstage(40)
Game.EnablePlayerControls()
Game.SetPlayerAIDriven(False)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
