Scriptname DSC_ClavicusVileMaskQuestScript extends Quest  

DSC_Utility Property util Auto
DSC_MCM Property config Auto
Actor Property PlayerRef Auto
Keyword Property DSC_ClavicusBlindfold Auto
Keyword Property DSC_ClavicusCollar Auto
Keyword Property DSC_ClavicusGag Auto
Keyword Property DSC_ClavicusMask Auto
GlobalVariable Property TimeScale Auto
GlobalVariable Property DSC_ClavicusVileTrickEquip Auto
GlobalVariable Property DSC_ClavicusVileHypnotised Auto
GlobalVariable Property DSC_ClavicusVileEventsDone Auto
GlobalVariable Property DSC_ClavicusVileAllowEscape Auto
Formlist Property DSC_SuppressedDevices Auto

Event OnInit()
  If IsRunning()
    RegisterForModEvent("DDi_Quest_SIGTERM", "Handle_SIGTERM")
    RegisterForModEvent("DSC_Quest_SIGTERM", "Handle_SIGTERM")
  EndIf
EndEvent

Event Handle_SIGTERM()
  UnequipMask()
  Stop()
EndEvent

Event OnUpdate()
  util.libs.NotifyPlayer("You feel the haze of the hypnosis lifting.")
  DSC_ClavicusVileHypnotised.SetValueInt(False as Int)
EndEvent

Event CVMaskOral(String eventName, String argString, Float argNum, Form sender)
  util.libs.NotifyPlayer("You feel the haze of the hypnosis lifting.")
  DSC_ClavicusVileHypnotised.SetValueInt(False as Int)
  IncreaseEvents()
EndEvent

Event CVMaskSex(String eventName, String argString, Float argNum, Form sender)
  util.libs.NotifyPlayer("You feel the haze of the hypnosis lifting.")
  DSC_ClavicusVileHypnotised.SetValueInt(False as Int)
  IncreaseEvents()
EndEvent

Event OnUpdateGameTime()
  UpdateState()
EndEvent

Function UpdateState()
  ;; Check if slot is currently active
  Bool BlindfoldState = PlayerRef.WornHasKeyword(DSC_ClavicusBlindfold)
  Bool CollarState = PlayerRef.WornHasKeyword(DSC_ClavicusCollar)
  Bool GagState = PlayerRef.WornHasKeyword(DSC_ClavicusGag)

  ;; Check if slot is occupied by another device.
  Bool OtherBlindfold = PlayerRef.WornHasKeyword(util.libs.zad_DeviousBlindfold) && !BlindfoldState
  Bool OtherCollar = PlayerRef.WornHasKeyword(util.libs.zad_DeviousCollar) && !CollarState
  Bool OtherGag = PlayerRef.WornHasKeyword(util.libs.zad_DeviousGag) && !GagState

  Int BlindfoldWeight = 0
  Int CollarWeight = 0
  Int GagWeight = 0

  ;; Only consider possible state if not occupied by other device.
  If !OtherBlindfold
    BlindfoldWeight = config.CVBlindWeight
  EndIf
  If !OtherCollar
    CollarWeight = config.CVCollarWeight
  EndIf
  If !OtherGag
    GagWeight = config.CVGagWeight
  EndIf

  Int TotalWeight = BlindfoldWeight + CollarWeight + GagWeight

  If TotalWeight == 0
    Debug.Trace("DSC: Clavicus Vile Mask: TotalWeight is zero; all slots are occupied, or the available slots have a weight of zero.")
  Else
    Keyword kw = util.libs.zad_DeviousHood
    Armor[] Devices = new Armor[2]
    Armor[] NewDevices = new Armor[2]
    String NotifyMessage

    ;; Here we select the device we should swap to, depending on the current active slots.
    Int r = Utility.RandomInt(1, TotalWeight)
    If r <= BlindfoldWeight
      ;; Set the devices to swap to.
      Devices = GetDeviceFromState(BlindfoldState, CollarState, GagState)
      NewDevices = GetDeviceFromState(!BlindfoldState, CollarState, GagState)

      ;; Set the message to be displayed
      If BlindfoldState
        String SighOrMoan
        If GagState || OtherGag ; WornHasKeyword isn't non-delayed, so quicker to just use booleans we already have.
          SighOrMoan = "moan into your gag"
        Else
          SighOrMoan = "sigh"
        EndIf
        NotifyMessage = "Slowly, the eyes on the mask start to open up again. After giving your eyes time to adjust, you " + SighOrMoan + " out of relief, as you are finally aware of your surroundings again."
      Else
        NotifyMessage = "Without warning, the eyes on the mask snap shut, leaving you blind and completely unaware of your surroundings."
      EndIf
    ElseIf r - BlindfoldWeight <= CollarWeight
      ;; Set the devices to swap to.
      Devices = GetDeviceFromState(BlindfoldState, CollarState, GagState)
      NewDevices = GetDeviceFromState(BlindfoldState, !CollarState, GagState)

      ;; Set the message to be displayed
      If CollarState
        NotifyMessage = "The leather around your neck starts loosening up, allowing you to finally take a deep breath of air again."
      Else
        NotifyMessage = "Without warning, the mask starts constricting around your neck, forming a sort of collar, leaving you lightwinded."
      EndIf
    Else
      ;; Set the devices to swap to.
      Devices = GetDeviceFromState(BlindfoldState, CollarState, GagState)
      NewDevices = GetDeviceFromState(BlindfoldState, CollarState, !GagState)

      ;; Set the message to be displayed
      If GagState
        NotifyMessage = "The mouthpiece on the mask starts shrinking again, finally allowing you to speak again and giving some rest to your aching jaw."
      Else
        NotifyMessage = "Without notice, the mouthpiece on the mask expands inwards, completely filling your mouth with a malleable tentacle that makes you unable to speak properly."
      EndIf
    EndIf

    ;; Swap to the new device.
    util.libs.RemoveQuestDevice(PlayerRef, Devices[0], Devices[1], kw, DSC_ClavicusMask, destroyDevice = True, skipMutex = True)
    util.libs.EquipDevice(PlayerRef, NewDevices[0], NewDevices[1], kw, skipMutex = True)
    ; util.libs.RemoveQuestDevice(PlayerRef, Devices[0], Devices[1], kw, DSC_ClavicusMask, destroyDevice = True)
    ; Utility.wait(0.1)
    ; util.libs.EquipDevice(PlayerRef, NewDevices[0], NewDevices[1], kw)

    ;; Display message to notify the player of what happened.
    Debug.Messagebox(NotifyMessage)
  EndIf

  Float MinTime = config.CVMinTimeUntilToggle
  Float MaxTime = config.CVMaxTimeUntilToggle

  Float r = Utility.RandomFloat(MinTime, MaxTime) as Float
  RegisterForSingleUpdateGameTime(r)
EndFunction

Function UnequipMask()
  DSC_ClavicusVileTrickEquip.SetValueInt(False as Int)

  ;; Check which slots are currently active
  Bool BlindfoldState = PlayerRef.WornHasKeyword(DSC_ClavicusBlindfold)
  Bool CollarState = PlayerRef.WornHasKeyword(DSC_ClavicusCollar)
  Bool GagState = PlayerRef.WornHasKeyword(DSC_ClavicusGag)

  Armor[] Devices = GetDeviceFromState(BlindfoldState, CollarState, GagState)
  Keyword kw = util.libs.zad_DeviousHood

  util.libs.RemoveQuestDevice(util.PlayerRef, Devices[0], Devices[1], kw, DSC_ClavicusMask, destroyDevice = True)
  util.PlayerRef.AddItem(util.DSC_ClavicusVileMaskInventory, abSilent = True)
EndFunction

Armor[] Function GetDeviceFromState(Bool BlindfoldState, Bool CollarState, Bool GagState)
  ;; Returns the device corresponding to the states given.
  ;; Would be better in a real language with metaprogramming, but it works.
  Armor[] DeviceTuple = new Armor[2]

  If     ! BlindfoldState && ! CollarState && ! GagState
    DeviceTuple[0] = util.DSC_ClavicusVileMaskInventory
    DeviceTuple[1] = util.DSC_ClavicusVileMaskRendered
  ElseIf ! BlindfoldState && ! CollarState &&   GagState
    DeviceTuple[0] = util.DSC_ClavicusVileMaskGInventory
    DeviceTuple[1] = util.DSC_ClavicusVileMaskGRendered
  ElseIf ! BlindfoldState &&   CollarState && ! GagState
    DeviceTuple[0] = util.DSC_ClavicusVileMaskCInventory
    DeviceTuple[1] = util.DSC_ClavicusVileMaskCRendered
  ElseIf ! BlindfoldState &&   CollarState &&   GagState
    DeviceTuple[0] = util.DSC_ClavicusVileMaskCGInventory
    DeviceTuple[1] = util.DSC_ClavicusVileMaskCGRendered
  ElseIf   BlindfoldState && ! CollarState && ! GagState
    DeviceTuple[0] = util.DSC_ClavicusVileMaskBInventory
    DeviceTuple[1] = util.DSC_ClavicusVileMaskBRendered
  ElseIf   BlindfoldState && ! CollarState &&   GagState
    DeviceTuple[0] = util.DSC_ClavicusVileMaskBGInventory
    DeviceTuple[1] = util.DSC_ClavicusVileMaskBGRendered
  ElseIf   BlindfoldState &&   CollarState && ! GagState
    DeviceTuple[0] = util.DSC_ClavicusVileMaskBCInventory
    DeviceTuple[1] = util.DSC_ClavicusVileMaskBCRendered
  ElseIf   BlindfoldState &&   CollarState &&   GagState
    DeviceTuple[0] = util.DSC_ClavicusVileMaskBCGInventory
    DeviceTuple[1] = util.DSC_ClavicusVileMaskBCGRendered
  EndIf

  Return DeviceTuple
EndFunction

Function EquipDevice()
  DSC_SuppressedDevices.Revert()
  DSC_SuppressedDevices.AddForm(util.libs.zad_DeviousHood)
  DSC_SuppressedDevices.AddForm(util.DSC_zad_DeviousHoodOpen)
  DSC_SuppressedDevices.AddForm(util.libs.zad_DeviousBlindfold)
  DSC_SuppressedDevices.AddForm(util.libs.zad_DeviousGag)
  DSC_SuppressedDevices.AddForm(util.libs.zad_DeviousCollar)

  util.EquipRandomDevice(util.PlayerRef, ownCustom = True)
  IncreaseEvents()

  ;; No longer used
;/  Keyword[] Pattern = new Keyword[11]
  Pattern[0]  = util.libs.zad_DeviousLegCuffs
  Pattern[1]  = util.libs.zad_DeviousArmCuffs
  Pattern[2]  = util.libs.zad_DeviousPiercingsNipple
  Pattern[3]  = util.libs.zad_DeviousPiercingsVaginal
  If config.GeneralUseChastity
    Pattern[4]  = util.libs.zad_DeviousBra
    Pattern[5]  = util.libs.zad_DeviousBelt
  EndIf
  Pattern[6]  = util.libs.zad_DeviousCorset
  Pattern[7]  = util.libs.zad_DeviousGloves
  Pattern[8]  = util.libs.zad_DeviousSuit
  Pattern[9]  = util.libs.zad_DeviousBoots
  If config.GeneralUseHeavy
    Pattern[10] = util.libs.zad_DeviousHeavyBondage
  EndIf

  util.ProgressiveBondage(util.PlayerRef, Pattern = Pattern)/;
EndFunction

Function IncreaseEvents()
  DSC_ClavicusVileEventsDone.SetValueInt(DSC_ClavicusVileEventsDone.GetValueInt() + 1)
  If DSC_ClavicusVileEventsDone.GetValueInt() >= config.CVEventsNeeded
    DSC_ClavicusVileAllowEscape.SetValueInt(True as Int)
    Debug.Notification("The mask's enchantment seems to have gotten weaker.")
  EndIf
EndFunction
