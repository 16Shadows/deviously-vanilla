;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_SS_TIF__0A003468 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
;Equip random generic gag based on selected tags
If config.DrevisGagDevice == "gag,ring"
  util.EquipRandomGagRing(PlayerRef)
ElseIf config.DrevisGagDevice == "gag,ball"
  util.EquipRandomGagBall(PlayerRef)
ElseIf config.DrevisGagDevice == "gag,panel"
  util.EquipRandomGagPanel(PlayerRef)
EndIf

;Equip straitjacket if enabled
If config.DrevisHeavyBondage
  util.EquipRandomHeavyBondage(PlayerRef, hobble = config.GeneralUseHobble, strictHobble = config.GeneralUseStrictHobble)
EndIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

zadlibs Property libs  Auto  

Actor Property PlayerRef  Auto  

DSC_MCM Property config  Auto  

zadxLibs Property libsx  Auto  

DSC_Utility Property Util  Auto  
