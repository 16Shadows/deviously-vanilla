;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0A0E7D34 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
utils.ManipulateDevice(utils.PlayerRef, utils.DSC_LodPetsuitInventory, unequip = true, destroyDevice = true)

Armor gag = utils.libs.GetWornDevice(utils.libs.PlayerRef, utils.libs.zad_DeviousGag)
if (gag != None && !(gag.HasKeyword(utils.libs.zad_BlockGeneric) || gag.HasKeyword(utils.libs.zad_QuestItem)))
utils.libs.ManipulateGenericDevice(utils.libs.PlayerRef, gag, false)
endif

utils.ManipulateDevice(utils.PlayerRef, utils.DSC_LodCollarInventory, force = true)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property utils  Auto