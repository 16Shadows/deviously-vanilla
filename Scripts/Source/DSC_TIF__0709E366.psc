;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0709E366 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
GetOwningQuest().setstage(30)

Keyword kw = util.libs.zad_DeviousSuit
Armor device = util.libs.GetWornDevice(PlayerRef, kw)
Armor deviceRendered = util.libs.GetRenderedDevice(device)
util.libs.RemoveDevice(PlayerRef, device, deviceRendered, kw)

;; Remove boots
util.libs.RemoveDevice(util.PlayerRef, util.libsx.zadx_SlaveHighHeelsRedInventory, util.libsx.zadx_SlaveHighHeelsRedRendered, util.libs.zad_DeviousBoots)

device = util.DSC_TemporaryDevices.GetAt(0) as Armor
deviceRendered = util.DSC_TemporaryDevicesRendered.GetAt(0) as Armor
kw = util.DSC_TemporaryDevicesKeywords.GetAt(0) as Keyword

util.libs.EquipDevice(util.PlayerRef, device, deviceRendered, kw)

util.DSC_TemporaryDevices.Revert()
util.DSC_TemporaryDevicesRendered.Revert()
util.DSC_TemporaryDevicesKeywords.Revert()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property LvlQuestReward01Small  Auto  

Actor Property PlayerRef  Auto  

DSC_Utility Property Util  Auto  
