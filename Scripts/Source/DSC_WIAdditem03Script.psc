Scriptname DSC_WIAdditem03Script extends Quest  

Event OnInit()
  Thug1 = Alias_Thug1.GetActorRef()
  Thug2 = Alias_Thug2.GetActorRef()
  Thug3 = Alias_Thug3.GetActorRef()

  RegisterForModEvent("AnimationEnd_DSCThug12", "StartSecondAnimations")
EndEvent

Event StartSecondAnimations(String eventName, String argString, Float argNum, Form sender)
  SexLabUtil.QuickStart(PlayerRef, Thug3, victim = PlayerRef, hook = "DSCThug3")
  ;RegisterForModEvent("AnimationEnd_DSCThug3", "StartThirdAnimations")
  RegisterForModEvent("AnimationEnd_DSCThug3", "AfterSexGreet")

  UnregisterForModEvent("AnimationEnd_DSCThug12")
EndEvent

;Event StartThirdAnimations(String eventName, String argString, Float argNum, Form sender)
;  SexLabUtil.QuickStart(PlayerRef, Thug1, Thug2, Thug3, victim = PlayerRef, hook = "DSCThug123")
;  RegisterForModEvent("AnimationEnd_DSCThug123", "StopThisQuest")
;
;  UnregisterForModEvent("AnimationEnd_DSCThug3")
;EndEvent

Event AfterSexGreet(String eventName, String argString, Float argNum, Form sender)
  AfterSexScene.Start()
EndEvent

Event StopThisQuest(String eventName, String argString, Float argNum, Form sender)
  SetStage(190)
EndEvent

Function FinishQuest()
	RegisterForSingleUpdate(1)
EndFunction

Event OnUpdate()
	self.SetStage(190)
EndEvent

ReferenceAlias Property Alias_Thug1  Auto
ReferenceAlias Property Alias_Thug2  Auto
ReferenceAlias Property Alias_Thug3  Auto
Actor Property PlayerRef  Auto  
Actor Thug1
Actor Thug2
Actor Thug3
Scene Property AfterSexScene Auto