Scriptname DSC_ClavicusVileMaskEventGagArouse Extends zadBaseEvent

Keyword Property DSC_ClavicusGag Auto

;; @Override
Bool Function HasKeywords(Actor akActor)
  Return akActor.WornHasKeyword(DSC_ClavicusGag) && akActor == libs.PlayerRef
EndFunction

;; @Override
Function Execute(Actor akActor)
  If akActor != libs.PlayerRef
    Return
  EndIf
  If libs.Aroused.GetActorArousal(akActor) >= 95
    libs.NotifyPlayer("The mask starts leaking a bitter fluid in your mouth, pulling you back to reality.", messagebox = true)
    libs.UpdateExposure(akActor, -30)
  Else
    Int r = Utility.RandomInt(0, 2)
    If r == 0
      libs.NotifyPlayer("The mask starts leaking a sweet fluid in your throat, and you find yourself getting a little more turned on.", messagebox = true)
      libs.UpdateExposure(akActor, 10)
    ElseIf r == 1
      libs.NotifyPlayer("The mask starts spraying copious amounts of sweet fluid in your mouth, struggling to swallow it all, you start getting really turned on.", messagebox = true)
      libs.UpdateExposure(akActor, 25)
    Else
      libs.NotifyPlayer("The gag in the mask starts forcing its way down your throat, and then deposits fluid almost right into your stomach. When it finally stops and you manage to catch your breath again, you notice your pussy is incredibly wet.", messagebox = true)
      libs.UpdateExposure(akActor, 75)
      akActor.DamageActorValue("Stamina", akActor.GetActorValue("Stamina") * 0.5)
      libs.CatchBreath(akActor)
    EndIf
  EndIf
EndFunction
