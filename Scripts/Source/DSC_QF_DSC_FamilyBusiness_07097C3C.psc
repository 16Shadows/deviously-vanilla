;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 6
Scriptname DSC_QF_DSC_FamilyBusiness_07097C3C Extends Quest Hidden

;BEGIN ALIAS PROPERTY ProventusAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_ProventusAlias Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY AdrianneAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_AdrianneAlias Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY UlfberthAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_UlfberthAlias Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY PlayerAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_PlayerAlias Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY PlayerBedroomMarkerAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_PlayerBedroomMarkerAlias Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_3
Function Fragment_3()
;BEGIN CODE
setObjectiveCompleted(30)
setObjectiveDisplayed(40)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_4
Function Fragment_4()
;BEGIN CODE
SetObjectiveCompleted(40)
DSC_FamilyBusinessAdrianneEnterScene.Start()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_1
Function Fragment_1()
;BEGIN CODE
SetObjectiveCompleted(10)
SetObjectiveDisplayed(20)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_5
Function Fragment_5()
;BEGIN CODE
PlayerRef.AddItem(FavorRewardGoldSmall)
DSC_FamilyBusinessDeviousWarmaidens.SetValueInt(True as Int)

SendModEvent("DHLP-Resume")

Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN AUTOCAST TYPE DSC_FamilyBusinessScript
Quest __temp = self as Quest
DSC_FamilyBusinessScript kmyQuest = __temp as DSC_FamilyBusinessScript
;END AUTOCAST
;BEGIN CODE
kmyQuest.EquipAdrianneDevices()
SetObjectiveDisplayed(10)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_2
Function Fragment_2()
;BEGIN CODE
ToBedroomScene.Start()
SetObjectiveCompleted(20)
SetObjectiveDisplayed(30)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Scene Property ToBedroomScene  Auto  

Scene Property DSC_FamilyBusinessAdrianneEnterScene  Auto  

Actor Property PlayerRef  Auto  

LeveledItem Property FavorRewardGoldSmall  Auto  

GlobalVariable Property DSC_FamilyBusinessDeviousWarmaidens  Auto  
