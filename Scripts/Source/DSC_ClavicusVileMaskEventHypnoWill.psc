Scriptname DSC_ClavicusVileMaskEventHypnoWill extends zadBaseEvent  

ImagespaceModifier Property HypnoImod Auto
GlobalVariable Property DSC_ClavicusVileEventsDone Auto
GlobalVariable Property DSC_ClavicusVileAllowEscape Auto
Keyword Property DSC_ClavicusBlindfold Auto

DSC_MCM Property config Auto

Spell Property DSC_ClavicusVileHypnoFortMagickaLow Auto
Spell Property DSC_ClavicusVileHypnoFortMagickaMed Auto
Spell Property DSC_ClavicusVileHypnoFortMagickaHigh Auto
Spell Property DSC_ClavicusVileHypnoDamMagickaLow Auto
Spell Property DSC_ClavicusVileHypnoDamMagickaMed Auto
Spell Property DSC_ClavicusVileHypnoDamMagickaHigh Auto

;; @Override
Bool Function HasKeywords(Actor akActor)
  Return akActor.WornHasKeyword(DSC_ClavicusBlindfold) && akActor == libs.PlayerRef
EndFunction

;; @Override
Function Execute(Actor akActor)
  If akActor != libs.PlayerRef
    Return
  EndIf

  libs.NotifyPlayer("Suddenly, the inside of the mask flashes some strange patterns,")
  HypnoImod.Apply(config.CVIModEnabled as Int)
  DSC_ClavicusVileEventsDone.SetValueInt(DSC_ClavicusVileEventsDone.GetValueInt() + 1)
  If DSC_ClavicusVileEventsDone.GetValueInt() > config.CVEventsNeeded
    DSC_ClavicusVileAllowEscape.SetValueInt(True as Int)
  EndIf

  Spell CurrentSpell
  Float WillpowerStrength

  Int Strength = Utility.RandomInt(0,2)
  Int GoodOrBad = Utility.RandomInt(0,2)
  If Strength == 0
    If GoodOrBad == 0
      WillpowerStrength = -2.0
      CurrentSpell = DSC_ClavicusVileHypnoDamMagickaLow
      libs.NotifyPlayer("and you feel your resolve weaken slightly.")
    Else
      WillpowerStrength = 2.0
      CurrentSpell = DSC_ClavicusVileHypnoFortMagickaLow
      libs.NotifyPlayer("and you feel your resolve strengthen slightly.")      
    EndIf
  ElseIf Strength == 1
    If GoodOrBad == 0
      WillpowerStrength = -5.0
      CurrentSpell = DSC_ClavicusVileHypnoDamMagickaMed
      libs.NotifyPlayer("and you feel your resolve weaken.")
    Else
      WillpowerStrength = 5.0
      CurrentSpell = DSC_ClavicusVileHypnoFortMagickaMed
      libs.NotifyPlayer("and you feel your resolve strengthen.")      
    EndIf
  ElseIf Strength == 2
    If GoodOrBad == 0
      WillpowerStrength = -10.0
      CurrentSpell = DSC_ClavicusVileHypnoDamMagickaHigh
      libs.NotifyPlayer("and you feel your resolve weaken greatly.")
    Else
      WillpowerStrength = 10.0
      CurrentSpell = DSC_ClavicusVileHypnoFortMagickaHigh
      libs.NotifyPlayer("and you feel your resolve strengthen greatly.")      
    EndIf
  EndIf

  CurrentSpell.Cast(libs.PlayerRef, libs.PlayerRef)

  If WillpowerStrength > 0
    SendModEvent("DF-ResistanceGain", "", WillpowerStrength)
  Else
    SendModEvent("DF-ResistanceLoss", "", WillpowerStrength)
  EndIf
EndFunction	