;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname DSC_TIF__0709E35D Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
libs.ManipulateDevice(libs.PlayerRef, libs.gagBall, equipOrUnequip = False)
DSC_SolitudeFreeform02TaarieGagEquippedReturn.SetValueInt(True as Int)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

zadlibs Property libs  Auto  

GlobalVariable Property DSC_SolitudeFreeform02TaarieGagEquippedReturn  Auto  
