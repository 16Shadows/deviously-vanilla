Scriptname ZWJ_V5_WabbajackScript extends ActiveMagicEffect

Explosion Property ZWJ_Wabbajack_Explosion Auto
FormList Property ZWJ_Wabbajack_PolymorphList Auto
Spell Property ZWJ_Wabbajack_PolymorphSpell Auto
Static Property XMarker Auto

Actor PolymorphActor
ActorBase PolymorphActorBase
ObjectReference MarkerRef

Event OnEffectStart(Actor akTarget, Actor akCaster)
DSC_Wabbajack DSC = Quest.GetQuest("DSC_Main") as DSC_Wabbajack

Bool AppliedDevices
If !DSC.RunOnPlayer()            ; Prevent from running DD on target if equipped on player
  AppliedDevices = DSC.RunOnTarget(akTarget)
EndIf


IF !AppliedDevices && (akTarget.GetActorValue("Health") > 0) && !(akTarget.IsDead())
MarkerRef = akTarget.PlaceAtMe(XMarker) as ObjectReference
	Int WhileCounter = 5
	WHILE WhileCounter > 0
		PolymorphActorBase = (ZWJ_Wabbajack_PolymorphList.GetAt(Utility.RandomInt(0, (ZWJ_Wabbajack_PolymorphList.GetSize() - 1)))) as ActorBase
		If (PolymorphActorBase.GetRace()) == (akTarget.GetRace())
			WhileCounter -= 1
		Else
			WhileCounter = 0
		EndIf
	ENDWHILE
	HideActor(akTarget)
	PolymorphActor = MarkerRef.PlaceAtMe(PolymorphActorBase) as Actor
	SetPercentageHealth(akTarget, PolymorphActor)
	akTarget.DoCombatSpellApply(ZWJ_Wabbajack_PolymorphSpell, PolymorphActor)
ENDIF
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
MarkerRef.MoveTo(PolymorphActor)
	Utility.Wait(0.1)
	HideActor(PolymorphActor)
	akTarget.EnableAI(True)
	akTarget.MoveTo(MarkerRef)
	akTarget.SetAlpha(1)
	akTarget.SetGhost(0)
	SetPercentageHealth(PolymorphActor, akTarget)
	Utility.Wait(1)
	PolymorphActor.Delete()
	MarkerRef.Delete()
EndEvent

Event OnActivate(ObjectReference akTriggerRef)
	If akTriggerRef == PolymorphActor
		Dispel()
	EndIf
EndEvent

Function SetPercentageHealth(Actor TemplateActor, Actor SettingActor)
	If TemplateActor.IsDead()
		SettingActor.DamageActorValue("Health", 9999)
	Else
		SettingActor.RestoreActorValue("Health", 9999)
		Float HealthPercentage = (TemplateActor.GetActorValue("Health")/TemplateActor.GetBaseActorValue("Health"))
		SettingActor.DamageActorValue("Health", (1-HealthPercentage)*(SettingActor.GetBaseActorValue("Health")) as Float)
	EndIf
EndFunction

Function HideActor(Actor akTarget)
	akTarget.Activate(akTarget)
	akTarget.SetAlpha(0)
	akTarget.PlaceAtMe(ZWJ_Wabbajack_Explosion)
	akTarget.MoveTo(akTarget,0,0,5000)
	akTarget.EnableAI(False)
	akTarget.SetGhost(1)
	akTarget.StopCombat()
EndFunction
