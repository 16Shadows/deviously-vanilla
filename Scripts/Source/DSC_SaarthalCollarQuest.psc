Scriptname DSC_SaarthalCollarQuest extends Quest  

Actor NerienRef

Event OnInit()
  If IsRunning()
    NerienRef = Alias_NerienAlias.GetActorRef()

    RegisterForModEvent("DDI_Quest_SigTerm", "DDiPanicRemoveDevice")
    RegisterForModEvent("DSC_Quest_SigTerm", "PanicRemoveDevice")
  EndIf
EndEvent

Function TriggerVision()
  ;; Should be fine without disable, but just more robust like this.
  NerienRef.DisableNoWait()

  Utility.Wait(0.5)
  NerienRef.MoveTo(utils.PlayerRef, 500.0 * Math.Sin(utils.PlayerRef.GetAngleZ()), 500.0 * Math.Cos(utils.PlayerRef.GetAngleZ()))

  ;; Run the freeze NPC quest
  DSC_SaarthalCollarMonkSceneQuest.Start()
 
  ;; Should be fine without, since it forcegreets, but for robustness sake, disable player controls.
  Game.DisablePlayerControls()

  NerienRef.Enable()
  NerienRef.SetAlpha(0)

  While !NerienRef.Is3DLoaded()
    Utility.Wait(0.1)
  EndWhile
  
  IntroFX.Apply()
  Utility.Wait(1.9)
  IntroFX.PopTo(LoopFX)
  PSGD.Apply(0.1)
  MGTeleportInEffect.Play(NerienRef, 3.6)
  NerienRef.SetAlpha(1, true)

  DSC_SaarthalCollarNerienScene.Start()
EndFunction

Function DisableVision()
  LoopFX.PopTo(OutroFX,fImodStrength)
  PSGD.Remove(1.0)
  MGTeleportOutEffect.Play(NerienRef)
  Utility.Wait(1.0)
  NerienRef.SetAlpha(0,true)
  Utility.Wait(1.0)
  NerienRef.Disable()
  DSC_SaarthalCollarMonkSceneQuest.Stop()
  Game.EnablePlayerControls()
EndFunction

Event NerienCollegeDisappear(String eventName, String argString, Float argNum, Form sender)
  DisableVision()

  SetStage(20)

  UnregisterForModEvent("AnimationEnd_NerienCollege")
EndEvent

Event ArchmageSexDone(String eventName, String argString, Float argNum, Form sender)
  AliasQuest.Stop()

  ArchmageUnlock()
EndEvent

Function ArchmageUnlock()
  Utility.Wait(1.0)
  Debug.Messagebox("Savos Aren suddenly casts a spell on you. You feel the collar releasing its grip on your neck, and it drops back into your hand, an amulet once again.")
  UnequipCollar()
  Stop()
EndFunction

Function TolfdirUnlock()
  Utility.Wait(1.0)
  Debug.Messagebox("After Tolfdir casts the spell on you, you feel the collar releasing its grip on your neck. It drops back into your hand, an amulet once again.")
  UnequipCollar()
  Stop()
EndFunction

Function CastSpellSelf()
  Debug.Messagebox("As you cast the spell, you feel the collar releasing its grip on your neck. It drops back into your hand, an amulet once again.")
  UnequipCollar()
  Stop()
EndFunction

Event DDiPanicRemoveDevice()
  ;; Seperate because DDi's safeword removes collar first, so we need to add the amulet back.
  Debug.Trace("DSC: DDi_SIGTERM received, removing collar")
  UnequipCollar()
  ;; Add the amulet back into the players inventory
  utils.PlayerRef.AddItem(GetCorrectAmulet(), abSilent = True)

;  AliasQuest.Stop()

;  Debug.Trace("DSC: DDi_SIGTERM received, ending quest")
;  Stop()
EndEvent

Event PanicRemoveDevice()
  Debug.Trace("DSC: DSC_SIGTERM received, removing collar")
  UnequipCollar()

;  AliasQuest.Stop()

;  Debug.Trace("DSC: DSC_SIGTERM received, ending quest")
;  Stop()
EndEvent

Function UnequipCollar()
  If utils.PlayerRef.WornHasKeyword(utils.DSC_SaarthalCollarKW)
    ;libs.RemoveQuestDevice(PlayerRef, DSC_SaarthalCollarInventory, DSC_SaarthalCollarRendered, Libs.zad_Deviouscollar, DSC_SaarthalCollarKeyword, destroyDevice = True)
    utils.ManipulateDevice(utils.PlayerRef, utils.DSC_SaarthalCollarInventory, unequip = true, destroyDevice = true)
    ;; Add the amulet back into the players inventory
    utils.PlayerRef.AddItem(GetCorrectAmulet(), abSilent = True)
  endif
  AliasQuest.Stop()
  Stop()
EndFunction

Armor Function GetCorrectAmulet()
  If utils.config.SCAllowRepeated
    return DSC_MG02Amulet
  Else
    return MG02Amulet
  EndIf
EndFunction

ImageSpaceModifier Property IntroFX  Auto  

ImageSpaceModifier Property LoopFX  Auto  

ImageSpaceModifier Property OutroFX  Auto  

Float Property fImodStrength  Auto  

ShaderParticleGeometry Property PSGD  Auto  

VisualEffect Property MGTeleportOutEffect  Auto  

VisualEffect Property MGTeleportInEffect  Auto  

ReferenceAlias Property Alias_NerienAlias  Auto  

Quest Property DSC_SaarthalCollarMonkSceneQuest  Auto  

Scene Property DSC_SaarthalCollarNerienScene  Auto  

DSC_Utility Property utils Auto

Armor Property MG02Amulet  Auto  

Armor Property DSC_MG02Amulet  Auto  

Quest Property AliasQuest  Auto  