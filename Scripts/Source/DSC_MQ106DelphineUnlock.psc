Scriptname DSC_MQ106DelphineUnlock extends Quest  

Actor Property DelphineRef Auto

GlobalVariable Property DSC_MQ106AfterSexStartPackage Auto

;Event OnInit()
;  
;EndEvent

Event DelphineUnlock(string eventName, string argString, float argNum, form sender)
  DSC_MQ106AfterSexStartPackage.SetValueInt(1)
  DelphineRef.EvaluatePackage()
  UnregisterForModEvent("AnimationEnd_DelphineUnlock")
EndEvent