Scriptname DSC_DelayedProgressQuest extends Quest  

int targetStage

Function DelayStage(float delay, int stage)
	targetStage = stage
	RegisterForSingleUpdateGameTime(delay)
EndFunction

Event OnUpdateGameTime()
	SetStage(targetStage)
endEvent
