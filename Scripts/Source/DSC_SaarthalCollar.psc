Scriptname DSC_SaarthalCollar extends zadCollarScript

DSC_SaarthalCollarQuest Property questScript  Auto

;; @override
Bool Function ShouldEquipSilently(Actor akActor)
  ;; We trick the player into equipping it, so it always equips silently.
  Return True
EndFunction

;; @override
Function OnEquippedPost(Actor akActor)
  questScript.Start()
EndFunction
