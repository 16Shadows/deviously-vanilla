Scriptname DSC_FollowerDetector extends Quest  

ReferenceAlias[] Property FollowersAlias  Auto  

int Function GetFollowersCount()
	int cnt = 0
	int i = 0

	While i < 10
		If FollowersAlias[i].GetActorRef() != None
			cnt += 1
		EndIf
		i += 1
	EndWhile

	return cnt
EndFunction

Actor[] Function GetActorsList()
	Actor[] actors = new Actor[10]
	int i = 0

	While i < 10
		actors[i] = FollowersAlias[i].GetActorRef()
		i += 1
	EndWhile
	
	return actors
EndFunction