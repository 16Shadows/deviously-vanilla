;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0709E361 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Keyword kw = util.libs.zad_DeviousGag
Armor device = util.libs.GetWornDevice(util.PlayerRef, kw)
Armor deviceRendered = util.libs.GetRenderedDevice(device)

util.DSC_TemporaryDevices.Revert()
util.DSC_TemporaryDevicesRendered.Revert()
util.DSC_TemporaryDevicesKeywords.Revert()

util.DSC_TemporaryDevices.AddForm(device)
util.DSC_TemporaryDevicesRendered.AddForm(deviceRendered)
util.DSC_TemporaryDevicesKeywords.AddForm(kw)

util.libs.RemoveDevice(util.PlayerRef, device, deviceRendered, kw)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property Util  Auto  
