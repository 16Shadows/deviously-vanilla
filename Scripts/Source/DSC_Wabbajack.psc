Scriptname DSC_Wabbajack extends Quest

DSC_Utility Property util Auto
DSC_MCM Property config Auto
zadlibs Property libs Auto
Actor Property PlayerRef Auto
Keyword Property keyWabbajackExcluded Auto

;; We use an async ModEvent rather than calling the functions inline,
;; since DD is very slow, which makes the Wabbajack effects unresponsive
Event DSC_Wabbajack_RunOnTargetEvent(Form akTarget)
  Actor victim = akTarget as Actor
  ;; Apply tags suppressed in MCM
  util.FillSuppressed(UseHeavy = config.WJUseHeavy, UseChastity = config.WJUseChastity, UseGag = config.WJUseGag, UseBlindfold = config.WJUseBlindfold, UseMittens = config.WJUseMittens, UseHobble = config.WJUseHobble, UseStrictHobble = config.WJUseStrictHobble)
  ; Keyword kw = util.GetRandomKeyword(victim)
  ; util.EquipRandomDeviceByKeyword(victim, kw)
  util.EquipRandomDevice(victim, ownCustom = True)
  UnregisterForModEvent("DSC_Wabbajack_RunOnTarget")
EndEvent

Bool Function RunOnPlayer()
  If config.WJChanceSelf != 0 && config.WJChanceSelf >= Utility.RandomFloat(0, 100)
    RegisterForModEvent("DSC_Wabbajack_RunOnTarget", "DSC_Wabbajack_RunOnTargetEvent")
    int handle = ModEvent.Create("DSC_Wabbajack_RunOnTarget")
    if handle
      ModEvent.PushForm(handle, PlayerRef)
      ModEvent.Send(handle)
    EndIf
    Return True
  EndIf
  Return False
EndFunction

Bool Function RunOnTarget(Actor akTarget)
  If config.WJChanceOthers != 0 && config.WJChanceOthers >= Utility.RandomFloat(0, 100)
    RegisterForModEvent("DSC_Wabbajack_RunOnTarget", "DSC_Wabbajack_RunOnTargetEvent")
  	Race TargetRace = akTarget.GetRace()
    ;; Check if race is enabled in MCM
    If TargetRace.isPlayable() && !akTarget.hasKeyword(keyWabbajackExcluded) && !(((TargetRace == Race.GetRace("OrcRace") || TargetRace == Race.GetRace("OrcVampireRace")) && !config.GeneralUseOrcs) || ((TargetRace == Race.GetRace("KhajiitRace") || TargetRace == Race.GetRace("KhajiitVampireRace")) && !config.GeneralUseKhajiit) || ((TargetRace == Race.GetRace("ArgonianRace") || TargetRace == Race.GetRace("ArgonianVampireRace")) && !config.GeneralUseArgonians))
      ;; Check if men are enabled in MCM
      If ( akTarget.GetLeveledActorBase() ).GetSex() || config.GeneralUseMales
        int handle = ModEvent.Create("DSC_Wabbajack_RunOnTarget")
        if handle
          ModEvent.PushForm(handle, akTarget)
            ModEvent.Send(handle)
          EndIf
        Return True             ; We applied devices
      EndIf
    EndIf
  EndIf
  Return False                  ; We didn't apply devices
EndFunction
