ScriptName DSC_UniversalEquip extends Quest

SexLabFramework Property SexLab Auto

Actor Property PlayerRef Auto

DSC_Utility Property util Auto

DSC_MCM Property config Auto

zadlibs Property libs Auto

Bool DisableOnce = False

Bool DisableIndefinitely = False

Event OnInit()
  RegisterForModEvent("AnimationStart_DefeatPvic", "DisableForDefeat")
  Debug.Trace("DSC: Defeat sexlab animation event registered")

  RegisterForModEvent("DHLP-Suspend", "Disable")
  RegisterForModEvent("DHLP-Resume", "Enable")

  RegisterForModEvent("AnimationEnd", "EquipAfterAnimation")
EndEvent


Event EquipAfterAnimation(string eventName, string argString, float argNum, form sender)
  If config.UEEnabled && !DisableOnce && !DisableIndefinitely
    sslThreadController controller = SexLab.HookController(argString)

    If controller.HasPlayer()
      Actor[] sexActors = SexLab.HookActors(argString)
      If config.UENeedPlayable && !util.HasPlayableRace(sexActors)
        Return
      EndIf

      util.FillSuppressed(UseHeavy = config.UEUseHeavy, UseChastity = config.UEUseChastity, UseGag = config.UEUseGag, UseBlindfold = config.UEUseBlindfold, UseMittens = config.UEUseMittens, UseHobble = config.UEUseHobble, UseStrictHobble = config.UEUseStrictHobble)

      sslBaseAnimation animation = SexLab.HookAnimation(argString)
      If controller.IsVictim(PlayerRef) || (config.UEUseTag && (animation.HasTag("Aggressive") || animation.HasTag("Rough")) && controller.GetPlayerPosition() == 0)
        ;; Player effects
        If config.UEChance >= Utility.RandomFloat(0, 100)
          util.EquipRandomDevice(PlayerRef, ownCustom = True)
        EndIf
      EndIf
    EndIf
  EndIf
  DisableOnce = False
EndEvent

Event DisableForDefeat(String EventName, String argString, Float argNum, Form sender)
  Debug.Trace("DSC: Defeat sexlab animation event received")
  If Game.GetModByName("mslDeviousCaptures.esp") != 255
    Debug.Trace("DSC: Devious Captures installed; disabling UniversalEquip")
    DisableOnce = True
  EndIf
EndEvent

Event Disable()
  DisableIndefinitely = True
EndEvent

Event Enable()
  DisableIndefinitely = False
EndEvent
