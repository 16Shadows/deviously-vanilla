;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname DSC_TIF__0A0E2C21 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
libs.EquipDevice(libs.PlayerRef, zadxItems.zadx_Gag_Bone_Black_Inventory, zadxItems.zadx_Gag_Bone_Black_Rendered, libs.zad_DeviousGag)
fails.SetValue(fails.GetValue()+1)
GetOwningQuest().SetStage(12)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

zadlibs Property libs  Auto  

zadxlibs2 Property zadxItems  Auto  

GlobalVariable Property Fails  Auto  
