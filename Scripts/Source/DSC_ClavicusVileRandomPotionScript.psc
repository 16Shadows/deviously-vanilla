Scriptname DSC_ClavicusVileRandomPotionScript extends ActiveMagicEffect

Bool Registered = False
Actor Property PlayerRef Auto
zadLibs Property libs Auto
Potion Property HealthPotion Auto
Potion Property MagickaPotion Auto
Potion Property StaminaPotion Auto

Event OnEffectStart(Actor akTarget, Actor akCaster)
  If akTarget != PlayerRef
    Return
  EndIf
  RegisterForUpdate(30)
EndEvent

Event OnUpdate()
  Float HealthPercent = PlayerRef.GetActorValuePercentage("Health")
  Float MagickaPercent = PlayerRef.GetActorValuePercentage("Magicka")
  Float StaminaPercent = PlayerRef.GetActorValuePercentage("Stamina")


  ;; Get lowest AV
  String MinAV = "Health"
  If HealthPercent > MagickaPercent
    MinAV = "Magicka"
    If MagickaPercent > StaminaPercent
      MinAV = "Stamina"
    EndIf
  EndIf
  If HealthPercent > StaminaPercent
    MinAV = "Stamina"
  EndIf

  If PlayerRef.GetActorValuePercentage(MinAV) < 0.33
    Libs.NotifyPlayer("The gag in your mouth feeds you a " + MinAV + " potion.")
    If MinAV == "Health"
      PlayerRef.EquipItem(HealthPotion, abSilent = True)
    ElseIf MinAV == "Magicka"
      PlayerRef.EquipItem(MagickaPotion, abSilent = True)
    ElseIf MinAV == "Stamina"
      PlayerRef.EquipItem(StaminaPotion, abSilent = True)
    EndIf
  EndIf
EndEvent

;; doesnt work cause skyrim is bad, just do it outside of combat
Event OnCombatStateChanged(Actor akTarget, Int aeCombatState)
  Debug.Trace("DSC: RandomPotionScript detected combat change")
  If aeCombatState != 0 && !Registered
    RegisterForUpdate(30)
    Registered = True
  Else
    UnregisterForUpdate()
    Registered = False
  EndIf
EndEvent
