;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__070A1F41 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Actor[] SexActors = new Actor[2]
SexActors[0] = PlayerRef
SexActors[1] = akSpeaker

sslBaseAnimation[] Anims
Int Type = DSC_ClavicusVileUnlockOfferType.GetValueInt()
If Type == 0
  Anims = SexLab.GetAnimationsByTags(2, "MF,vaginal")
ElseIf Type == 1
  Anims = SexLab.GetAnimationsByTags(2, "oral")
ElseIf Type == 3
  Anims = SexLab.GetAnimationsByTags(2, "MF,anal")
EndIf

Sexlab.StartSex(SexActors, Anims)

GetOwningQuest().Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

SexLabFramework Property SexLab  Auto  

Actor Property PlayerRef  Auto  

GlobalVariable Property DSC_ClavicusVileUnlockOfferType  Auto  
