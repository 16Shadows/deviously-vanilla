;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__070A19AC Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
;; Check if speaker has penis
DSC_ClavicusVileCurrentSpeakerSLGender.SetValueInt(SexLab.GetGender(akSpeaker))

;; to prevent player from simply quitting out and starting new dialogue
;; no longer used
DSC_ClavicusVileHypnotised.SetValueInt(True as Int)
(GetOwningQuest() as DSC_ClavicusVileMaskQuestScript).RegisterForSingleUpdate(60)

;; Apply visuals and message
HypnoImod.Apply(config.CVIModEnabled as Int)
libs.NotifyPlayer("The inside of the masque flashes some strange patterns,")
libs.NotifyPlayer("and you feel a haze fall over your mind.")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_MCM Property config  Auto  

ImageSpaceModifier Property HypnoImod  Auto  

zadlibs Property libs  Auto  

GlobalVariable Property DSC_ClavicusVileHypnotised  Auto  

GlobalVariable Property DSC_ClavicusVileEventsDone  Auto  

GlobalVariable Property DSC_ClavicusVileAllowEscape  Auto  

GlobalVariable Property DSC_ClavicusVileCurrentSpeakerSLGender  Auto  

SexLabFramework Property SexLab  Auto  
