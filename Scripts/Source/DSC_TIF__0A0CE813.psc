;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0A0CE813 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
;utils.PlayerRef.UnequipAll()
utils.PlayerRef.UnequipItemSlot(32)
utils.PlayerRef.UnequipItemSlot(33)
utils.PlayerRef.UnequipItemSlot(37)
utils.PlayerRef.UnequipItemSlot(30)
utils.ManipulateDevice(utils.PlayerRef, utils.DSC_LodPetsuitInventory)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property utils  Auto  
