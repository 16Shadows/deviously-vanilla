Scriptname DSC_SaarthalCollarPlayerScript extends ReferenceAlias  

Event OnLocationChange(Location akOldLoc, Location akNewLoc)
;  Debug.Trace("DSC Saarthal Collar: Player changed location to " + akNewLoc.GetName() + " from " + akOldLoc.GetName())
  If GetOwningQuest().GetStage() == 10
;    Debug.Trace("DSC: quest stage correct")
    If akNewLoc == WinterholdCollegeHallOfElementsLocation
;      Debug.Trace("DSC: starting vision")
      questScript.TriggerVision()
    EndIf
  EndIf
EndEvent

DSC_SaarthalCollarQuest Property QuestScript  Auto  

Location Property WinterholdCollegeHallofElementsLocation  Auto  
