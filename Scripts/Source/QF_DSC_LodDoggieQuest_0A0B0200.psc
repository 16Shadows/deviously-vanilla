;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 12
Scriptname QF_DSC_LodDoggieQuest_0A0B0200 Extends Quest Hidden

;BEGIN ALIAS PROPERTY LodAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_LodAlias Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_10
Function Fragment_10()
;BEGIN CODE
SetObjectiveCompleted(15)
self.Reset()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_7
Function Fragment_7()
;BEGIN CODE
if IsObjectiveDisplayed(10)
SetObjectiveCompleted(10)
Elseif IsObjectiveDisplayed(11)
SetObjectiveCompleted(11)
endif

SetObjectiveDisplayed(15)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_8
Function Fragment_8()
;BEGIN CODE
fails.SetValue(0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property Fails  Auto  
