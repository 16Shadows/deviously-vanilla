;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0707D0FB Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Armor device = util.DSC_TemporaryDevices.GetAt(0) as Armor
Armor deviceRendered = util.DSC_TemporaryDevicesRendered.GetAt(0) as Armor
Keyword kw = util.DSC_TemporaryDevicesKeywords.GetAt(0) as Keyword

util.libs.EquipDevice(PlayerRef, device, deviceRendered, kw)

util.DSC_TemporaryDevices.Revert()
util.DSC_TemporaryDevicesRendered.Revert()
util.DSC_TemporaryDevicesKeywords.Revert()

DSC_SolitudeFreeform02GagEquippedAtElisif.SetValueInt(False as Int)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

DSC_Utility Property Util  Auto  

GlobalVariable Property DSC_SolitudeFreeform02GagEquippedAtElisif  Auto  
