;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0709D890 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
libs.ManipulateGenericDeviceByKeyword(libs.PlayerRef, libs.zad_DeviousHeavyBondage, false)

Actor[] sexActors = new Actor[3]
sexActors[0] = PlayerRef
sexActors[1] = akSpeaker
sexActors[2] = UlfberthRef

;sslBaseAnimation[] anims = SexLab.GetAnimationsByType(3, males = 2, females = 1)
sslBaseAnimation[] anims = SexLab.GetAnimationsByTags(3, "MMF,bound")

If anims.length == 0
  anims = SexLab.GetAnimationsByType(3, males = 2, females = 1)
EndIf

(GetOwningQuest() as DSC_FamilyBusinessScript).RegisterForModEvent("AnimationEnd_AdrianneUlfberthBondageSex", "AdrianneUlfberthBondageSex")

SexLab.StartSex(sexActors, anims, hook = "AdrianneUlfberthBondageSex")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

Actor Property UlfberthREF  Auto  

SexLabFramework Property SexLab  Auto  

zadlibs Property libs  Auto  
