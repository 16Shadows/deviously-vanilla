Scriptname DSC_ClavicusVileMaskEventGagDildo extends zadBaseLinkedEvent

Keyword Property DSC_ClavicusGag Auto

Bool Function HasKeywords (Actor akActor)
  return akActor.WornHasKeyword(DSC_ClavicusGag) && akActor == libs.PlayerRef
EndFunction

Function Execute(Actor akActor)
  If akActor != libs.PlayerRef
    Return
  EndIf
  If Utility.RandomInt(0,4) == 0
    libs.NotifyPlayer("The gag in your mouth starts forcing it's way through your mouth,")
    libs.NotifyPlayer("and once it's reached the back, it suddenly start fucking your throat.")
    Utility.Wait(Utility.RandomInt(15, 20))
    libs.NotifyPlayer("The gag, seemingly satisfied, retracts to its normal size again.")
  Else
    libs.NotifyPlayer("The gag in your mouth caresses your mouth,")
    libs.NotifyPlayer("then wrestling with your tongue like a dominant lover.")
    Utility.Wait(Utility.RandomInt(5, 10))
    If Utility.RandomInt(0,2) == 0
      libs.NotifyPlayer("The gag breaks your kiss, returning to normal again.")
    Else
      libs.NotifyPlayer("The gag, after completely overpowering your tongue,")
      libs.NotifyPlayer("starts creeping its way through your mouth")
      libs.NotifyPlayer("and once it's reached the back, it suddenly start fucking your throat.")
      Utility.Wait(Utility.RandomInt(15, 20))
      libs.NotifyPlayer("The gag, seemingly satisfied, retracts to its normal size again.")
    EndIf
  EndIf
EndFunction
