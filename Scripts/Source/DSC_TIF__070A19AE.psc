;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname DSC_TIF__070A19AE Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
DSC_ClavicusVileMaskQuestScript QuestScript = GetOwningQuest() as DSC_ClavicusVileMaskQuestScript
QuestScript.UnregisterForUpdate()
QuestScript.EquipDevice()
Debug.Notification("The haze over your mind lifts again")
DSC_ClavicusVileHypnotised.SetValueInt(False as Int)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property DSC_ClavicusVileHypnotised  Auto  
