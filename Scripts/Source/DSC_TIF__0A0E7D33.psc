;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0A0E7D33 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
utils.ManipulateDevice(utils.PlayerRef, utils.DSC_LodPetsuitInventory, unequip = true, destroyDevice = true)

utils.libs.EquipDevice(utils.libs.PlayerRef, utils.libsx2.zadx_PetSuit_Ebonite_Black_Inventory, utils.libsx2.zadx_PetSuit_Ebonite_Black_Rendered, utils.libs.zad_DeviousPetSuit, true)

utils.ManipulateDevice(utils.PlayerRef, utils.DSC_LodCollarInventory, force = true)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property utils  Auto