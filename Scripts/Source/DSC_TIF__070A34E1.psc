;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__070A34E1 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
util.libs.ManipulateDevice(util.PlayerRef, util.libs.gagBall, equipOrUnequip = True)
Game.GetPlayer().AddItem(LvlQuestReward01Small)
akSpeaker.SetRelationshipRank( Game.GetPlayer(), 1 ) ;USKP 2.1.3 Bug #19403
GetOwningQuest().Setstage(30)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property Util  Auto  

LeveledItem Property LvlQuestReward01Small  Auto  
