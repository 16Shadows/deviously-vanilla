;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_PF_DSC_ConjureStormAtrona_0707A057 Extends Package Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(Actor akActor)
;BEGIN CODE
Utility.Wait(1.0)
AliasQuest.Start()

Utility.Wait(0.1)

Actor PlayerRef = Alias_PlayerAlias.GetActorRef()
Actor StormAtronachRef = Alias_StormAtronachAlias.GetActorRef()

(GetOwningQuest() as DSC_SaarthalCollarQuest).RegisterForModEvent("AnimationEnd_ArchmageSexEnd", "ArchmageSexDone")
SexLabUtil.QuickStart(PlayerRef, StormAtronachRef, hook = "ArchmageSexEnd")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ReferenceAlias Property Alias_PlayerAlias  Auto  

ReferenceAlias Property Alias_StormAtronachAlias  Auto  

Quest Property AliasQuest  Auto  
