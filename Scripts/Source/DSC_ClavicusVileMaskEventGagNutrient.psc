Scriptname DSC_ClavicusVileMaskEventGagNutrient extends zadBaseEvent

Keyword Property DSC_ClavicusGag Auto
Potion Property FoodNutrient Auto
Potion Property DrinkNutrient Auto

;; @Override
Bool Function HasKeywords(Actor akActor)
  Return akActor == libs.PlayerRef && akActor.WornHasKeyword(DSC_ClavicusGag)
EndFunction

;; @Override
Function Execute(Actor akActor)
  If akActor != libs.PlayerRef
    Return
  EndIf

  libs.NotifyPlayer("The tentacle gag in your mouth feeds you some bitter tasting nutrients")

  libs.PlayerRef.EquipItem(FoodNutrient, abSilent = True)
  libs.PlayerRef.EquipItem(DrinkNutrient, abSilent = True)
EndFunction
