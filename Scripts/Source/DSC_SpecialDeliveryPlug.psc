Scriptname DSC_SpecialDeliveryPlug extends zadPlugScript  

Function RemovePlugNoLock()
	Parent.RemovePlugNoLock()
	If DSC_Quest.GetState() != "EffectOn"
		return
	EndIf
	DSC_unequipMessage.Show()
	If !libs.PlayerRef.WornHasKeyword(DSC_plugKW)
		DSC_Quest.GoToState("EffectOff")
	Endif
EndFunction

DSC_FreeformIvarstead04 Property DSC_Quest Auto
Message Property DSC_unequipMessage Auto
Keyword Property DSC_plugKW Auto