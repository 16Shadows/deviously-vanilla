;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__07098C98 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
(GetOwningQuest() as DSC_FamilyBusinessScript).RegisterForModEvent("AnimationEnd_UlfberthRape", "UlfberthAfterRape")

Actor[] sexActors = new Actor[2]
sexActors[0] = PlayerRef
sexActors[1] = akSpeaker

sslBaseAnimation[] anims

SexLab.StartSex(sexActors, anims, victim = PlayerRef, centerOn = sexMarker, hook = "UlfberthRape")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

ObjectReference Property sexMarker Auto

SexLabFramework Property SexLab Auto
