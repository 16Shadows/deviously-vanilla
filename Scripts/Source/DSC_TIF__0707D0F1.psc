;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname DSC_TIF__0707D0F1 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
;util.EquipRandomDeviceByKeyword(PlayerRef, util.libs.zad_DeviousGag)
util.libs.ManipulateDevice(PlayerRef, util.libs.gagBall, equipOrUnequip = True)
GetOwningQuest().SetStage(11)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

DSC_Utility Property Util  Auto  
