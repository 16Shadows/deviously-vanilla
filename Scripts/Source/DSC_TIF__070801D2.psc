;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__070801D2 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
util.EquipRandomHeavyBondage(PlayerRef, hobble = config.GeneralUseHobble, strictHobble = config.GeneralUseStrictHobble)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

DSC_Utility Property Util  Auto  

Actor Property PlayerRef  Auto  

DSC_MCM Property config  Auto  
