;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__070A1F45 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Int Count = PlayerRef.GetItemCount(Gold001)
If Count > 300
  Count = 300
EndIf
PlayerRef.RemoveItem(Gold001, Count)

SexLabUtil.QuickStart(PlayerRef, akSpeaker, victim = PlayerRef)

GetOwningQuest().Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

MiscObject Property Gold001  Auto  
