Scriptname DSC_FamilyBusinessScript extends Quest  

DSC_Utility Property util Auto
Actor Property PlayerRef Auto
Scene Property DSC_FamilyBusinessUlfberthAfterRapeScene Auto
Scene Property DSC_FamilyBusinessAdrianneUlfberthBondageSexScene Auto
Scene Property DSC_FamilyBusinessAdrianneUlfberthSexAfterScene Auto

Event OnInit()
;  If DSC_FamilyBusinessAdrianneUlfberthSexScene == None
;    If Game.GetModByName("Deviously Vanilla.esp") != 255
;      DSC_FamilyBusinessAdrianneUlfberthSexAfterScene = Game.GetFormFromFile(0x, "Deviously Vanilla.esp")
;    ElseIf Game.GetModByName("Deviously Vanilla - AIO.esp") != 255
;      DSC_FamilyBusinessAdrianneUlfberthSexAfterScene = Game.GetFormFromFile(0x, "Deviously Vanilla - AIO.esp")
;    EndIf
;  EndIf
EndEvent

Function EquipAdrianneDevices()
  util.ManipulateDevice(PlayerRef, util.DSC_AdrianneArmCuffsInventory, force = True)
  util.ManipulateDevice(PlayerRef, util.DSC_AdrianneLegCuffsInventory, force = True)
  util.ManipulateDevice(PlayerRef, util.DSC_AdrianneCollarInventory, force = True)
EndFunction

Function EquipUlfberthBinder()
  util.ManipulateDevice(PlayerRef, util.DSC_UlfberthArmbinderInventory, force = True)
EndFunction

Function LoosenUlfberthBinder()
  util.libs.RemoveDevice(util.PlayerRef, util.DSC_UlfberthArmbinderInventory, util.DSC_UlfberthArmbinderRendered, util.libs.zad_DeviousHeavyBondage, destroyDevice = true, skipMutex = true)
  util.libs.EquipDevice(util.PlayerRef, util.DSC_UlfberthArmbinderLooseInventory, util.DSC_UlfberthArmbinderLooseRendered, util.libs.zad_DeviousHeavyBondage, skipMutex = true)
  SetStage(45)
EndFunction

Function EquipUlfberthPlugs()
  util.ManipulateDevice(util.PlayerRef, util.DSC_BlackSoulgemVagInventory, force = True)
  util.ManipulateDevice(util.PlayerRef, util.DSC_BlackSoulgemAnInventory, force = True)
  util.VibrateEffect(util.PlayerRef, 5, 10, teaseOnly = True)
EndFunction

Event AdrianneUlfberthBondageSex(String eventName, String argString, Float argNum, Form sender)
  DSC_FamilyBusinessAdrianneUlfberthBondageSexScene.Start()
EndEvent

Event AdrianneUlfberthSex(String eventName, String argString, Float argNum, Form sender)
  DSC_FamilyBusinessAdrianneUlfberthSexAfterScene.Start()
EndEvent

Event UlfberthAfterRape(string eventName, string argString, float argNum, form sender)
  DSC_FamilyBusinessUlfberthAfterRapeScene.Start()
EndEvent