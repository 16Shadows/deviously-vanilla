;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0A0F7043 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Actor[] actors = new Actor[2]
actors[0] = PlayerRef
actors[1] = akSpeaker

sslBaseAnimation[] anims = SexLab.GetAnimationsByTags(2, "Doggy,Doggystyle,Doggy Style", false)

(GetOwningQuest() as DSC_LodDoggieQuestScript).RegisterForModEvent("AnimationEnd_LodDoggieSex", "OnSceneEnd")

SexLab.StartSex(actors, anims, hook = "LodDoggieSex")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

SexLabFramework Property SexLab  Auto  

Actor Property PlayerRef  Auto  
