;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname DSC_PF_DSC_ConjureDremoraPack_0707A04A Extends Package Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(Actor akActor)
;BEGIN CODE
Utility.Wait(1.0)
AliasQuest.Start()

Utility.Wait(0.1)

Actor PlayerRef = Alias_PlayerAlias.GetActorRef()
Actor DremoraRef = Alias_DremoraAlias.GetActorRef()

(GetOwningQuest() as DSC_SaarthalCollarQuest).RegisterForModEvent("AnimationEnd_ArchmageSexEnd", "ArchmageSexDone")
SexLabUtil.QuickStart(PlayerRef, DremoraRef, hook = "ArchmageSexEnd")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ReferenceAlias Property Alias_PlayerAlias  Auto  

ReferenceAlias Property Alias_DremoraAlias  Auto  

Quest Property AliasQuest  Auto  
