;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__07077A5E Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Actor OpponentRef = Alias_Opponent.GetActorRef()

Actor[] SexActors = new Actor[2]
SexActors[0] = PlayerRef
SexActors[1] = OpponentRef

sslBaseAnimation[] anims
anims = SexLab.GetAnimationsByTag(SexActors.length, "Aggressive")


SexLab.StartSex(SexActors, anims, victim=PlayerRef)

;; Stop the brawling quest
GetOwningQuest().Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

ReferenceAlias Property Alias_Opponent  Auto  

SexLabFramework Property SexLab  Auto  
