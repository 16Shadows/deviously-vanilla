;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname DSC_SS_TIF__0B00AA03 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
SSv2Quest.YsoldaSex = 1
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
util.EquipRandomGag(PlayerRef)

SexLabUtil.QuickStart(PlayerRef, YsoldaRef, victim = PlayerRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ssv2questscript Property SSv2Quest  Auto  

DSC_Utility Property Util  Auto  

Actor Property PlayerRef  Auto  

Actor Property YsoldaREF  Auto  
