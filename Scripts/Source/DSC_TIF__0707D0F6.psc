;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname DSC_TIF__0707D0F6 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
PlayerRef.RemoveItem(DSC_FitForAJarlTaarieNote)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
DSC_SolitudeFreeform02GagEquippedAtElisif.SetValue(True as Float)

Keyword kw = util.libs.zad_DeviousGag
Armor device = util.libs.GetWornDevice(PlayerRef, kw)
Armor deviceRendered = util.libs.GetRenderedDevice(device)

util.DSC_TemporaryDevices.Revert()
util.DSC_TemporaryDevicesRendered.Revert()
util.DSC_TemporaryDevicesKeywords.Revert()

util.DSC_TemporaryDevices.AddForm(device)
util.DSC_TemporaryDevicesRendered.AddForm(deviceRendered)
util.DSC_TemporaryDevicesKeywords.AddForm(kw)

util.libs.RemoveDevice(PlayerRef, device, deviceRendered, kw)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

DSC_Utility Property Util  Auto  

GlobalVariable Property DSC_SolitudeFreeform02GagEquippedAtElisif  Auto  

Book Property DSC_FitForAJarlTaarieNote  Auto  
