Scriptname DSC_SpecialDeliveryBeltScript extends zadBeltScript  

Function DeviceMenuRemoveWithKey()
	If RemoveDeviceWithKey()
		DSC_BeltEndMessage.Show()
		DSC_Quest.SetStage(51)
	EndIf
EndFunction

Function DeviceMenuPickLock()
	int unlockChance = libs.CheckDeviceEscape(libs.GetUnlockThreshold(), "Lockpicking")
	string out = ""
        if (unlockChance == -1)
		out += "You carefully insert a lockpick into the keyhole and manage to pick the lock. "
		out += DeviceMenuPickLockSuccess()
		DSC_BeltEndMessage.Show()
		DSC_Quest.SetStage(51)
	elseif (unlockChance >= 100)
		out += "Despite your best lock breaking efforts the device refuse to open. "
		DeviceMenuPickLockModerate()
	else
		out += "Whether it's the lack of practice or just bad luck, the lock remains shut, breaking your lockpick in the process. "
		out += DeviceMenuPickLockFail()
	endif
	libs.Notify(out, messageBox=true)
EndFunction

DSC_FreeformIvarstead04 Property DSC_Quest  Auto  

Message Property DSC_BeltEndMessage  Auto  