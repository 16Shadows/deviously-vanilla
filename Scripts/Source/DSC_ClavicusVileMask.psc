Scriptname DSC_ClavicusVileMask extends zadEquipScript

Quest Property TheQuest Auto
GlobalVariable Property DSC_ClavicusVileTrickEquip Auto
DSC_MCM Property config Auto

;; @Override
Bool Function ShouldEquipSilently(Actor akActor)
  If DSC_ClavicusVileTrickEquip.GetValue()
    Return True
    DSC_ClavicusVileTrickEquip.SetValueInt(False as Int)
  EndIf
  Parent.ShouldEquipSilently(akActor)
EndFunction

;; @Override
Function OnEquippedPost(Actor akActor)
  If !TheQuest.IsRunning()
    TheQuest.Start()

    Float MinTime = config.CVMinTimeUntilToggle
    Float MaxTime = config.CVMaxTimeUntilToggle

    Float r = Utility.RandomFloat(MinTime, MaxTime) as Float
    (TheQuest as DSC_ClavicusVileMaskQuestScript).RegisterForSingleUpdateGameTime(r)
  EndIf
EndFunction

;; @Override
int Function OnEquippedFilter(actor akActor, bool silent=false)
  If akActor != libs.PlayerRef
    Return 2
  Else
    Parent.OnEquippedFilter(akActor, Silent)
  EndIf
EndFunction
