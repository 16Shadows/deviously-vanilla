;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__0707B09A Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Actor Thug1 = Alias_Thug1.GetActorRef()
Actor Thug2 = Alias_Thug2.GetActorRef()
Actor Thug3 = Alias_Thug3.GetActorRef()

SexLabUtil.QuickStart(PlayerRef, Thug1, Thug2, victim = PlayerRef, hook = "DSCThug12")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Actor Property PlayerRef  Auto  

ReferenceAlias Property Alias_Thug1  Auto  

ReferenceAlias Property Alias_Thug2  Auto  

ReferenceAlias Property Alias_Thug3  Auto  
