;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname DSC_TIF__070A1F2A Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Actor[] SexActors = New Actor[2]
SexActors[0] = PlayerRef
SexActors[1] = akSpeaker

sslBaseAnimation[] anims = SexLab.GetAnimationsByType(2, males = 1, females = 1)

(GetOwningQuest() as DSC_ClavicusVileMaskQuestScript).RegisterForModEvent("AnimationEnd_CVMaskSex", "CVMaskSex")
(GetOwningQuest() as DSC_ClavicusVileMaskQuestScript).UnregisterForUpdate()

SexLab.StartSex(SexActors, anims, hook = "CVMaskSex")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

SexLabFramework Property SexLab  Auto  

Actor Property PlayerRef  Auto  
