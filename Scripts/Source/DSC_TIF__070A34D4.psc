;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname DSC_TIF__070A34D4 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
;equip boots
libs.EquipDevice(libs.PlayerRef, libsx.zadx_SlaveHighHeelsRedInventory, libsx.zadx_SlaveHighHeelsRedRendered, libs.zad_DeviousBoots)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

zadlibs Property libs  Auto  

zadxLibs Property libsx  Auto  
