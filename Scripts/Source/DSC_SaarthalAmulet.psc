Scriptname DSC_SaarthalAmulet extends ObjectReference

zadlibs Property libs Auto
Armor Property DSC_SaarthalCollarInventory Auto
Armor Property MG02Amulet Auto
DSC_Utility Property util Auto

Event OnEquipped(Actor akActor)
  If akActor == Game.GetPlayer()
    Debug.Messagebox("As you put on the amulet, you feel it expanding around your neck. It starts getting tighter, restricting your ability to breathe. You grab at it with your hands and notice it turned into a steel posture collar.")


    akActor.addItem(DSC_SaarthalCollarInventory, abSilent = True)
    util.EquipDevice(akActor, DSC_SaarthalCollarInventory, force = True)
    akActor.RemoveItem(MG02Amulet, abSilent = True)
  EndIf
EndEvent
