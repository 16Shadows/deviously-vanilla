;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 4
Scriptname DSC_QF_DSC_SaarthalCollarQues_07079550 Extends Quest Hidden

;BEGIN ALIAS PROPERTY PlayerAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_PlayerAlias Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY NerienAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_NerienAlias Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY SavosAlias
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_SavosAlias Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN CODE
;; Quest start
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_1
Function Fragment_1()
;BEGIN AUTOCAST TYPE DSC_SaarthalCollarQuest
Quest __temp = self as Quest
DSC_SaarthalCollarQuest kmyQuest = __temp as DSC_SaarthalCollarQuest
;END AUTOCAST
;BEGIN CODE
;; Spoken with Nerien in Saarthal
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_3
Function Fragment_3()
;BEGIN AUTOCAST TYPE DSC_SaarthalCollarQuest
Quest __temp = self as Quest
DSC_SaarthalCollarQuest kmyQuest = __temp as DSC_SaarthalCollarQuest
;END AUTOCAST
;BEGIN CODE
kmyquest.DisableVision()
kmyquest.UnequipCollar()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_2
Function Fragment_2()
;BEGIN CODE
;; Spoken with Nerien at College
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
