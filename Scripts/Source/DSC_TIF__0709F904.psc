;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 3
Scriptname DSC_TIF__0709F904 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_2
Function Fragment_2(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
DSC_ClavicusVileHypnotised.SetValueInt(True as Int)
(GetOwningQuest() as DSC_ClavicusVileMaskQuestScript).RegisterForSingleUpdate(60)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ImageSpaceModifier Property HypnoImod  Auto  

GlobalVariable Property DSC_ClavicusVileHypnotised  Auto  

DSC_MCM Property config  Auto  
